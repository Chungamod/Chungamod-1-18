package magnileve.chungamod;

import java.util.Objects;

/**
 * A {@code PluginID} identifies a plugin for Chungamod that instances of {@link magnileve.chungamod.modules.ModuleID ModuleID} belong to.
 * @author Magnileve
 */
public record PluginID(String id, String displayName, String version) implements Comparable<PluginID> {

/**
 * Creates a new {@code PluginID} with the given properties.
 * @param id identifier unique to all loaded plugins
 * @param displayName display name
 * @param version currently loaded version
 */
public PluginID(String id, String displayName, String version) {
	this.id = Objects.requireNonNull(id);
	this.displayName = Objects.requireNonNull(displayName);
	this.version = version;
}

@Override
public boolean equals(Object obj) {
	if(obj instanceof PluginID) return ((PluginID) obj).id.equals(id);
	return false;
}

@Override
public int hashCode() {
	return id.hashCode();
}

@Override
public String toString() {
	return version == null ? displayName : displayName + ' ' + version;
}

/**
 * Compares the name of this plugin to another lexicographically.
 * @param o the plugin to be compared
 * @return a negative integer, zero, or a positive integer as this plugin's name
 * is lexicographically less than, equal to, or greater than that of the specified plugin.
 */
@Override
public int compareTo(PluginID o) {
	return id.compareTo(o.id);
}

}