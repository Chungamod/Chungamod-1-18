package magnileve.chungamod;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.List;
import java.util.Set;

import org.objectweb.asm.tree.ClassNode;
import org.spongepowered.asm.mixin.extensibility.IMixinConfigPlugin;
import org.spongepowered.asm.mixin.extensibility.IMixinInfo;

/**
 * A Spongepowered mixin plugin that starts loading and applies mixins of Chungamod plugins.
 * @author Magnileve
 */
public class ChungamodSponge implements IMixinConfigPlugin {

@Override
public void onLoad(String mixinPackage) {
	ClassLoader knotClassLoader = ChungamodSponge.class.getClassLoader();
	Method method;
	try {
		method = knotClassLoader.getClass().getMethod("addUrlFwd", URL.class);
	} catch (NoSuchMethodException e) {
		throw new Error("Unable to access Fabric class loader", e);
	}
	method.setAccessible(true);
	PluginLoader.instance.loadPlugins(url -> {
		try {
			method.invoke(knotClassLoader, url);
		} catch (IllegalAccessException | InvocationTargetException e) {
			if(e instanceof InvocationTargetException) throw new Error("Exception adding jar " + url + " to class loader", e.getCause());
		}
	});
}

@Override
public String getRefMapperConfig() {
	return null;
}

@Override
public boolean shouldApplyMixin(String targetClassName, String mixinClassName) {
	return false;
}

@Override
public void acceptTargets(Set<String> myTargets, Set<String> otherTargets) {}

@Override
public List<String> getMixins() {
	return null;
}

@Override
public void preApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {}

@Override
public void postApply(String targetClassName, ClassNode targetClass, String mixinClassName, IMixinInfo mixinInfo) {}

}