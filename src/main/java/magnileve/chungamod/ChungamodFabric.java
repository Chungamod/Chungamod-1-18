package magnileve.chungamod;

import magnileve.chungamod.events.EventManager;
import magnileve.chungamod.events.EventPoster;
import magnileve.chungamod.events.types.ChatMessageEvent;
import magnileve.chungamod.events.types.ConnectionEvent;
import magnileve.chungamod.events.types.GuiEvent;
import magnileve.chungamod.events.types.SwitchWorldEvent;
import magnileve.chungamod.modules.ContainsInit;
import magnileve.chungamod.modules.Init;
import net.fabricmc.api.ClientModInitializer;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.world.ClientWorld;

/**
 * Manages interaction with the Fabric Mod Loader.
 * This class should be considered internal.
 * @author Magnileve
 * @see magnileve.chungamod.mixins
 */
@ContainsInit
public class ChungamodFabric implements ClientModInitializer {

private static MinecraftClient mc;
private static EventPoster<ConnectionEvent> connectionEvents;
private static EventPoster<SwitchWorldEvent> worldEvents;
private static EventPoster<ChatMessageEvent> chatEvents;
private static EventPoster<GuiEvent> guiEvents;

private static boolean setScreenNull;

@Init
static void init(MinecraftClient mcIn, EventManager eventManager) {
	mc = mcIn;
	connectionEvents = eventManager.registerEvent(ConnectionEvent.class);
	worldEvents = eventManager.registerEvent(SwitchWorldEvent.class);
	chatEvents = eventManager.registerEvent(ChatMessageEvent.class);
	guiEvents = eventManager.registerEvent(GuiEvent.class);
}

@Override
public void onInitializeClient() {
	Chung.US.init();
}

/**
 * Called by mixin when joining a world.
 * @param world the loading world
 */
public static void onJoinWorld(ClientWorld world) {
	if(mc.world == null) {
		connectionEvents.post(new ConnectionEvent(world));
		Tick.MAIN.add(() -> {
			Chung.US.connectModules();
			return -1;
		});
	} else worldEvents.post(new SwitchWorldEvent(world));
}

/**
 * Called by mixin when leaving a world without joining another.
 */
public static void onLeaveWorld() {
	Chung.US.disconnectModules();
	connectionEvents.post(new ConnectionEvent(null));
}

/**
 * Called by mixin when a chat message is about to be sent to the server.
 * @param message the chat message
 * @returns {@code true} if the post to chat should be cancelled; {@code false} otherwise
 */
public static boolean postChatMessage(String message) {
	return chatEvents.post(new ChatMessageEvent(message));
}

/**
 * Called by mixin right before the current GUI screen is changed
 * @param screen the new screen
 * @return the new screen or a screen that has been set by an event listener
 */
public static Screen onSetScreen(Screen screen) {
	if(setScreenNull) {
		if(screen == null) return null;
		else setScreenNull = false;
	}
	GuiEvent event = new GuiEvent(screen, true);
	guiEvents.post(event);
	while(event.isScreenReplaced()) {
		event = new GuiEvent(event.getScreen(), false);
		guiEvents.post(event);
	}
	Screen result = event.getScreen();
	if(result == null) setScreenNull = true;
	return result;
}

}