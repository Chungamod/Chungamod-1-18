package magnileve.chungamod.mixins;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import magnileve.chungamod.ChungamodFabric;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.world.ClientWorld;

@Mixin(MinecraftClient.class)
public class WorldLifecycle {

@Inject(method = "joinWorld(Lnet/minecraft/client/world/ClientWorld;)V", at = @At("HEAD"))
private void onJoinWorld(ClientWorld world, CallbackInfo info) {
	ChungamodFabric.onJoinWorld(world);
}

@Inject(method = "disconnect(Lnet/minecraft/client/gui/screen/Screen;)V", at = @At("HEAD"))
private void onLeaveWorld(Screen screen, CallbackInfo info) {
	if(((MinecraftClient) (Object) this).world != null) ChungamodFabric.onLeaveWorld();
}

}