package magnileve.chungamod.mixins;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import magnileve.chungamod.Tick;
import net.minecraft.client.MinecraftClient;

@Mixin(MinecraftClient.class)
public class ClientTickListener {

@Inject(method = "tick()V", at = @At("HEAD"))
private void preClientTick(CallbackInfo info) {
	Tick.MAIN.onTick();
}

@Inject(method = "tick()V", at = @At("TAIL"))
private void postClientTick(CallbackInfo info) {
	Tick.CLIENT_POST.onTick();
}

}