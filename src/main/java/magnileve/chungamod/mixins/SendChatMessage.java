package magnileve.chungamod.mixins;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import magnileve.chungamod.ChungamodFabric;
import net.minecraft.client.network.ClientPlayerEntity;

@Mixin(ClientPlayerEntity.class)
public class SendChatMessage {

@Inject(method = "sendChatMessage(Ljava/lang/String;)V", at = @At("HEAD"), cancellable = true)
private void sendChatMessage(String message, CallbackInfo info) {
	if(ChungamodFabric.postChatMessage(message)) info.cancel();
}

}