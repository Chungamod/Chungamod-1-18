package magnileve.chungamod.mixins;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import io.netty.channel.ChannelHandlerContext;
import magnileve.chungamod.packets.PacketListener;
import net.minecraft.network.ClientConnection;
import net.minecraft.network.NetworkSide;
import net.minecraft.text.Text;

@Mixin(ClientConnection.class)
public class NetworkConnection {

@Inject(method = "channelActive(Lio/netty/channel/ChannelHandlerContext;)V", at = @At("TAIL"))
private void onChannelCreation(ChannelHandlerContext context, CallbackInfo info) {
	if(((ClientConnection) (Object) this).getSide().equals(NetworkSide.CLIENTBOUND)) PacketListener.onConnect(context.channel());
}

@Inject(method = "disconnect(Lnet/minecraft/text/Text;)V", at = @At("HEAD"))
private void onDisconnect(Text text, CallbackInfo info) {
	if(((ClientConnection) (Object) this).getSide().equals(NetworkSide.CLIENTBOUND)) PacketListener.onDisconnect();
}

}