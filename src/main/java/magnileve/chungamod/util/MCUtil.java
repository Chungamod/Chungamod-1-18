package magnileve.chungamod.util;

import magnileve.chungamod.modules.ContainsInit;
import magnileve.chungamod.modules.Init;
import net.minecraft.client.MinecraftClient;
import net.minecraft.entity.Entity;
import net.minecraft.text.LiteralText;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;

/**
 * Contains static utility methods involving the Mincerfat code.
 * @author Magnileve
 */
@ContainsInit
public class MCUtil {

private static MinecraftClient mc;

@Init
public static void init(MinecraftClient mcIn) {
	mc = mcIn;
}

private MCUtil() {}

/**
 * Gets the {@code BlockPos} an entity is currently within.  This is done by rounding an entity position to floor.
 * @param e an entity
 * @return the {@code BlockPos} the given entity is currently within
 */
public static BlockPos getPos(Entity e) {
	return new BlockPos(e.getPos());
}

/**
 * Gets the {@code BlockPos} the player is currently within.  This is done by rounding the player's position to floor.
 * @return the {@code BlockPos} the player is currently within
 */
public static BlockPos playerPos() {
	return new BlockPos(mc.player.getPos());
}

/**
 * Sends a client-side message in chat.
 * @param message message to be displayed
 * @throws NullPointerException if there is no current chat GUI
 */
public static void sendMessage(String message) {
	mc.inGameHud.getChatHud().addMessage(new LiteralText("[Chungamod] " + message));
}

/**
 * Sends a client-side message in chat if it is possible to do so.
 * @param message message to be displayed
 */
public static void trySendMessage(String message) {
	if(mc.inGameHud != null) sendMessage(message);
}

/**
 * Parses a {@code BlockPos} from a {@code String}.
 * @param coords input coordinates
 * @return a {@code BlockPos} containing these coordinates
 * @throws IllegalArgumentException if {@code coords} is not of length {@code 3},
 * or relative coordinates are used when a player does not exist
 * @throws NumberFormatException if an input string does not contain a parsable integer
 */
public static BlockPos parseCoords(String[] coords) {
	if(coords.length != 3) throw new IllegalArgumentException("Format: <x> <y> <z>");
	Vec3d camera = null;
	int[] ints = new int[3];
	for(int i = 0; i < 3; i++) {
		if(coords[i].startsWith("~")) {
			if(camera == null) {
				camera = mc.getCameraEntity().getPos();
				if(camera == null) throw new IllegalArgumentException("Cannot use relative coordinates when player is null");
			}
			ints[i] = (int) (i == 0 ? camera.x : i == 1 ? camera.y : camera.z) + Integer.parseInt(coords[i].substring(1));
		}
		ints[i] = Integer.parseInt(coords[i]);
	}
	return new BlockPos(ints[0], ints[1], ints[2]);
}

/**
 * Gets a vector one unit away from the origin in the given direction.
 * @param pitch pitch of rotation
 * @param yaw yaw of rotation
 * @return vector with rotation of the input pitch and yaw
 */
public static Vec3d getVectorForRotation(float pitch, float yaw) {
	float f = - MathHelper.cos(pitch * -0.017453292F);
	return new Vec3d((double) (MathHelper.sin(yaw * -0.017453292F - (float) Math.PI) * f),
			(double) MathHelper.sin(pitch * -0.017453292F),
			(double) (MathHelper.cos(yaw * -0.017453292F - (float) Math.PI) * f));
}

/**
 * Disconnects the client from the server, resulting in a disconnection screen being displayed.
 * @param reason the text to be displayed on the disconnection screen
 */
public static void disconnect(String reason) {
	mc.getNetworkHandler().getConnection().disconnect(new LiteralText(reason));
}

}