package magnileve.chungamod.util;

import java.util.Objects;

/**
 * A {@code Container} contains one mutable reference to an object.
 * @author Magnileve
 * @param <T> reference type
 */
public final class Container<T> implements Comparable<Container<T>> {

private T value;

public Container(T value) {
	this.value = value;
}

public Container() {}

public T get() {
	return value;
}

public void set(T value) {
	this.value = value;
}

@Override
public String toString() {
	return "Container[" + value + ']';
}

@Override
public boolean equals(Object obj) {
	return obj instanceof Container<?> c ? Objects.equals(value, c.value) : false;
}

@Override
public int hashCode() {
	return value == null ? 0 : value.hashCode();
}

/**
 * Compares this container with the specified container for order.
 * {@code null} values are considered to be less than any non-null value.
 * @throws ClassCastException if both containers have non-null values and
 * this container's value does not implement {@code Comparable} or
 * implements it with a type parameter incompatible with the other container's value
 */
@SuppressWarnings("unchecked")
@Override
public int compareTo(Container<T> o) {
	return value == null ? o.value == null ? 0 : -1 : o.value == null ? 1 : ((Comparable<T>) value).compareTo(o.value);
}

}