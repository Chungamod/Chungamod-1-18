package magnileve.chungamod.util.function;

import java.util.Objects;

/**
 * Represents a function that accepts three int-valued arguments and returns no
 * result.  This is the {@code int}-consuming primitive specialization for
 * {@link magnileve.chungamod.util.function.TriConsumer TriConsumer}.
 */
@FunctionalInterface
public interface TriIntConsumer {

/**
 * Performs this operation on the given arguments.
 * @param value1 the first input argument
 * @param value2 the second input argument
 * @param value3 the third input argument
 */
public void accept(int value1, int value2, int value3);

/**
 * Returns a composed {@code TriIntConsumer} that performs, in sequence, this
 * operation followed by the {@code after} operation. If performing either
 * operation throws an exception, it is relayed to the caller of the
 * composed operation.  If performing this operation throws an exception,
 * the {@code after} operation will not be performed.
 *
 * @param after the operation to perform after this operation
 * @return a composed {@code TriIntConsumer} that performs in sequence this
 * operation followed by the {@code after} operation
 * @throws NullPointerException if {@code after} is null
 */
default TriIntConsumer andThen(TriIntConsumer after) {
    Objects.requireNonNull(after);
    return (v1, v2, v3) -> {
    	accept(v1, v2, v3);
    	after.accept(v1, v2, v3);
    };
}

}