package magnileve.chungamod.util.function;

import java.util.Objects;

/**
 * Represents a function that accepts two double-valued arguments and returns no
 * result.  This is the {@code double}-consuming primitive specialization for
 * {@link java.util.function.BiConsumer BiConsumer}.
 */
@FunctionalInterface
public interface BiDoubleConsumer {

/**
 * Performs this operation on the given arguments.
 * @param value1 the first input argument
 * @param value2 the second input argument
 */
public void accept(double value1, double value2);

/**
 * Returns a composed {@code BiDoubleConsumer} that performs, in sequence, this
 * operation followed by the {@code after} operation. If performing either
 * operation throws an exception, it is relayed to the caller of the
 * composed operation.  If performing this operation throws an exception,
 * the {@code after} operation will not be performed.
 *
 * @param after the operation to perform after this operation
 * @return a composed {@code BiDoubleConsumer} that performs in sequence this
 * operation followed by the {@code after} operation
 * @throws NullPointerException if {@code after} is null
 */
default BiDoubleConsumer andThen(BiDoubleConsumer after) {
    Objects.requireNonNull(after);
    return (v1, v2) -> {
    	accept(v1, v2);
    	after.accept(v1, v2);
    };
}

}