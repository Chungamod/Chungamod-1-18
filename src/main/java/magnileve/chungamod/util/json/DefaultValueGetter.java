package magnileve.chungamod.util.json;


@FunctionalInterface
public interface DefaultValueGetter<T> {

public T getDefault(String limits) throws DefaultValueNotSupportedException;

public static <T> DefaultValueGetter<T> notSupported(Class<T> type, String limiterId) {
	return limits -> {
		throw new DefaultValueNotSupportedException(type, limiterId);
	};
}

}