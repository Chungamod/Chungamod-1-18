package magnileve.chungamod.util.json;

/**
 * Thrown to indicate that a value does not satisfy its required limits.
 * @author Magnileve
 * @see ValueLimiter
 */
public class InvalidValueException extends Exception {

private static final long serialVersionUID = -7791364909084668302L;

/** Constructs a new invalid value exception with {@code null} as its
 * detail message.  The cause is not initialized, and may subsequently be
 * initialized by a call to {@link #initCause}.
 */
public InvalidValueException() {
    super();
}

/** Constructs a new invalid value exception with the specified detail message.
 * The cause is not initialized, and may subsequently be initialized by a
 * call to {@link #initCause}.
 *
 * @param   message   the detail message. The detail message is saved for
 *          later retrieval by the {@link #getMessage()} method.
 */
public InvalidValueException(String message) {
    super(message);
}

/**
 * Constructs a new invalid value exception with the specified detail message and
 * cause.  <p>Note that the detail message associated with
 * {@code cause} is <i>not</i> automatically incorporated in
 * this invalid value exception's detail message.
 *
 * @param  message the detail message (which is saved for later retrieval
 *         by the {@link #getMessage()} method).
 * @param  cause the cause (which is saved for later retrieval by the
 *         {@link #getCause()} method).  (A {@code null} value is
 *         permitted, and indicates that the cause is nonexistent or
 *         unknown.)
 */
public InvalidValueException(String message, Throwable cause) {
    super(message, cause);
}

/** Constructs a new invalid value exception with the specified cause and a
 * detail message of {@code (cause==null ? null : cause.toString())}
 * (which typically contains the class and detail message of
 * {@code cause}).  This constructor is useful for invalid value exceptions
 * that are little more than wrappers for other throwables.
 *
 * @param  cause the cause (which is saved for later retrieval by the
 *         {@link #getCause()} method).  (A {@code null} value is
 *         permitted, and indicates that the cause is nonexistent or
 *         unknown.)
 */
public InvalidValueException(Throwable cause) {
    super(cause);
}

}