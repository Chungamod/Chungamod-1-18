package magnileve.chungamod.tasks;

import magnileve.chungamod.modules.Module;

public abstract class TaskModule extends AbstractTask implements Module, Runnable {

@Override
public void run() {
	start();
}

@Override
public void start(Runnable completionCallback) {
	super.start(completionCallback == null ? this::selfDisable : () -> {
		selfDisable();
		completionCallback.run();
	});
}

@Override
public void disable() {
	if(isActive()) cancel();
}

}