package magnileve.chungamod.tasks;

import magnileve.chungamod.Tick;
import magnileve.chungamod.TickListener;

public abstract class TickListenerTask extends AbstractTask {

private final Tick tick;
private final TickListener listener;
private final int initialTickDelay;

public TickListenerTask(Tick tickType, int initialTickDelay) {
	tick = tickType;
	listener = () -> {
		int i = onTick();
		if(i < 0) finish();
		return i;
	};
	this.initialTickDelay = initialTickDelay;
}

public TickListenerTask(Tick tickType) {
	this(tickType, 0);
}

public TickListenerTask() {
	this(Tick.MAIN, 0);
}

protected abstract int onTick();

@Override
protected void started() {
	tick.add(listener, initialTickDelay);
}

@Override
protected void cancelled() {
	tick.remove(listener);
}

@Override
protected void disconnected() {
	tick.remove(listener);
}

}