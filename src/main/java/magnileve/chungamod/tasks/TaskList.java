package magnileve.chungamod.tasks;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.List;

public class TaskList extends TaskQueue {

private final List<Task> tasks;

public TaskList(Collection<Task> tasks) {
	super(new ArrayDeque<>());
	this.tasks = List.copyOf(tasks);
}

@Override
public void started() {
	queue.clear();
	queue.addAll(tasks);
	super.started();
}

}