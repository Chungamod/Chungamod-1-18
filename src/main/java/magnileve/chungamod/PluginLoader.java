package magnileve.chungamod;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Consumer;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONTokener;
import org.spongepowered.asm.mixin.Mixins;

/**
 * The {@code PluginLoader} is an an internal class that begins the loading process of Chungamod plugins.
 * A singleton instance is created when mixins are being applied to Chungamod and
 * removed after the module loading process in {@link Chung} has been started.
 * @author Magnileve
 */
class PluginLoader {

static final Set<String> MOD_SERVICES = Set.of();

/**
 * Singleton instance of {@link PluginLoader} when plugins are being loaded.
 */
static PluginLoader instance = new PluginLoader(LogManager.getLogger(Chung.MODID));

final Map<String, LoadingPlugin> loadingPlugins;
final Map<String, Set<String>> loadingServices;

private final Logger log;

private PluginLoader(Logger log) {
	this.log = log;
	loadingPlugins = new HashMap<>();
	loadingServices = new HashMap<>();
}

/**
 * Loads all plugins into instances of {@link LoadingPlugin}.
 * @param addJarToClassLoader consumes a URL of a plugin jar and adds it to the mod loader's class loader
 */
void loadPlugins(Consumer<URL> addJarToClassLoader) {
	log.debug("Loading plugin jars");
	
	//get list of plugin jars
	Path pluginsDirectory = Paths.get(Chung.CHUNGAMOD_DIRECTORY, Chung.PLUGINS_DIRECTORY);
	List<File> jars;
	try {
		if(!Files.exists(pluginsDirectory)) Files.createDirectories(pluginsDirectory);
		jars = Files.list(pluginsDirectory)
				.filter(path -> Files.isRegularFile(path) && path.getFileName().toString().endsWith(".jar"))
				.map(path -> new File(path.toString()))
				.collect(Collectors.toList());
		
	} catch (IOException e) {
		throw new UncheckedIOException("Unable to open plugins directory: " + pluginsDirectory, e);
	}
	
	//iterate through plugin jars
	Map<String, LoadingPlugin> loadingPlugins = new HashMap<>();
	Map<String, Set<String>> loadingServices = new HashMap<>();
	Set<String> usableServices = new HashSet<>(MOD_SERVICES);
	Map<String, String> moduleClasses = new HashMap<>();
	Set<String> preLoadClasses = new HashSet<>();
	for(File jar:jars) try(JarFile jarFile = new JarFile(jar)) {
		log.debug("Reading jar {}", jar);
		ZipEntry pluginJson = jarFile.getEntry("chungamod.json");
		if(pluginJson == null) continue;
		
		@SuppressWarnings("unchecked")
		class NewLoadingPlugin {
			LoadingPlugin pl;
			List<String> moduleBasePackages = Collections.EMPTY_LIST;
			List<String> modulePackages = Collections.EMPTY_LIST;
		}
		
		//parse chungamod.json
		List<NewLoadingPlugin> newLoadingPlugins = new ArrayList<>();
		LoadingPlugin all = new LoadingPlugin(null, null);
		try(InputStream input = jarFile.getInputStream(pluginJson)) {
			JSONTokener p = new JSONTokener(new BufferedReader(new InputStreamReader(input), input.available()));
			for(String pluginId = jsonObjectStart(p); pluginId != null; pluginId = jsonObjectNext(p)) {
				LoadingPlugin pl = pluginId.equals("all") ? all : new LoadingPlugin(all, pluginId);
				NewLoadingPlugin npl = new NewLoadingPlugin();
				npl.pl = pl;
				for(String mainKey = jsonObjectStart(p); mainKey != null; mainKey = jsonObjectNext(p)) switch(mainKey) {
				case "displayName": {
					char c = p.nextClean();
					if(c != '\"' && c != '\'') throw p.syntaxError("Unable to parse String");
					pl.displayName = p.nextString(c);
					break;
				} case "version": {
					char c = p.nextClean();
					if(c != '\"' && c != '\'') throw p.syntaxError("Unable to parse String");
					pl.version = p.nextString(c);
					break;
				} case "load": {
					for(String key = jsonObjectStart(p); key != null; key = jsonObjectNext(p)) switch(key) {
					case "moduleBasePackages": {
						npl.moduleBasePackages = readJsonStringArray(p);
						break;
					} case "modulePackages": {
						npl.modulePackages = readJsonStringArray(p);
						break;
					} case "mixins": {
						pl.mixinFiles = readJsonStringArray(p);
						break;
					} case "preLoadClasses": {
						for(String className:readJsonStringArray(p)) {
							recordClass(moduleClasses, className, pl.id);
							preLoadClasses.add(className);
						}
						break;
					}
					default: p.nextValue();
					}
					break;
				} case "dependencies": {
					pl.dependencies = readJsonStringArray(p);
					break;
				} case "services": {
					if(p.nextClean() != '[') throw p.syntaxError("Unable to parse array");
					if(p.nextClean() == ']') break;
					p.back();
					do {
						String service = null;
						String provider = null;
						List<String> uses = null;
						for(String key = jsonObjectStart(p); key != null; key = jsonObjectNext(p)) {
							char c = p.nextClean();
							if(c == '[' && "uses".equals(key)) {
								p.back();
								uses = readJsonStringArray(p);
								continue;
							}
							if(c != '\"' && c != '\'') throw p.syntaxError("Unable to parse service provider");
							switch(key) {
							case "provides": {
								service = p.nextString(c);
								break;
							} case "with": {
								provider = p.nextString(c);
								break;
							}
							default: throw p.syntaxError("Unable to parse service provider");
							}
						}
						if(uses == null && service != null && provider != null) {
							String providerInMap = provider;
							loadingServices.compute(service, (serviceInMap, providers) -> {
								if(providers == null) providers = new HashSet<>();
								providers.add(providerInMap);
								return providers;
							});
						} else if(uses != null && service == null && provider == null) usableServices.addAll(uses);
						else throw p.syntaxError("Unable to parse service provider");
					} while(nextJsonArrayElement(p));
					break;
				}
				default: p.nextValue();
				}
				newLoadingPlugins.add(npl);
			}
		}
		
		//find and list classes from moduleBasePackages and modulePackages
		Enumeration<JarEntry> iter = jarFile.entries();
		while(iter.hasMoreElements()) {
			JarEntry e = iter.nextElement();
			String fileName = e.getName();
		    if(!e.isDirectory() && fileName.endsWith(".class")) {
		    	String name = fileName.substring(0, fileName.length() - 6).replace('/', '.');
		    	String packageName = name.substring(0, name.lastIndexOf('.'));
		    	for(NewLoadingPlugin npl:newLoadingPlugins) {
		    		for(String modulePackage:npl.modulePackages) if(packageName.equals(modulePackage)) {
		    			recordClass(moduleClasses, name, npl.pl.id);
		    			npl.pl.moduleClasses.add(name);
		    		}
		    		for(String basePackage:npl.moduleBasePackages) if(packageName.startsWith(basePackage) &&
		    				(packageName.length() == basePackage.length() || packageName.charAt(basePackage.length()) == '.')) {
		    			recordClass(moduleClasses, name, npl.pl.id);
		    			npl.pl.moduleClasses.add(name);
		    		}
		    	}
		    }
		}
		
		//add loading plugins from this jar to loading plugin map
		for(NewLoadingPlugin npl:newLoadingPlugins) {
			loadingPlugins.merge(npl.pl.id, npl.pl, (p1, p2) -> {
				throw new PluginLoadingError("Duplicate plugin name: " + p1.id);
			});
		}
		
		//add jar to class loader
		addJarToClassLoader.accept(jar.toURI().toURL());
	} catch (IOException | RuntimeException e) {
		throw new PluginLoadingError("Error reading plugin jar " + jar, e);
	}
	
	//apply mixins of plugins and check for any missing dependencies
	Map<String, List<String>> missing = new HashMap<>();
	for(LoadingPlugin pl:loadingPlugins.values()) {
		for(String dependency:pl.getDependencies()) if(!loadingPlugins.containsKey(dependency)) missing.compute(dependency, (dep, dependents) -> {
			if(dependents == null) dependents = new ArrayList<>();
			dependents.add(pl.id);
			return dependents;
		});
		log.debug("Applying mixins for " + pl.id);
		Mixins.addConfigurations(pl.getMixinFiles().toArray(new String[0]));
	}
	if(!missing.isEmpty()) throw new MissingPluginError(missing);
	
	//add loading plugins and loading services from this method to PluginLoader instance
	this.loadingPlugins.putAll(loadingPlugins);
	for(Entry<String, Set<String>> entry:loadingServices.entrySet()) {
		String key = entry.getKey();
		if(usableServices.contains(key)) this.loadingServices.put(key, entry.getValue());
	}
	
	//Load any pre-load classes specified by plugin jars
	if(!preLoadClasses.isEmpty()) {
		log.debug("Loading pre-load classes");
		for(String className:preLoadClasses) try {
			Class.forName(className);
		} catch (Throwable e) {
			throw new PluginLoadingError("Error loading plugin pre-load classes", e);
		}
	}
}

/**
 * Records to a map that a class belongs to a plugin.
 * @param classes map of classes to plugin IDs
 * @param className name of class
 * @param pluginId ID of plugin owning the class
 * @throws PluginLoadingError if, according to the map, the given class already belongs to a different plugin
 */
private static void recordClass(Map<String, String> classes, String className, String pluginId) {
	classes.merge(className, pluginId, (p1, p2) -> {
		if(p1.equals(p2)) return p1;
		throw new PluginLoadingError("Class " + className + " is declared by multiple plugins: " + p1 + " and " + p2);
	});
}

private static List<String> readJsonStringArray(JSONTokener p) {
	List<String> strs = new ArrayList<>();
	if(p.nextClean() != '[') throw p.syntaxError("Unable to parse String array");
	if(p.nextClean() == ']') return strs;
	p.back();
	do {
		char c = p.nextClean();
		if(c != '\"' && c != '\'') throw p.syntaxError("Unable to parse String array");
		strs.add(p.nextString(c));
	} while(nextJsonArrayElement(p));
	return strs;
}

private static boolean nextJsonArrayElement(JSONTokener p) {
	char c = p.nextClean();
	if(c == ',') return true;
	if(c == ']') return false;
	throw p.syntaxError("Unable to parse JSON array");
}

private static String jsonObjectStart(JSONTokener p) {
	char c = p.nextClean();
	if(c != '{') throw p.syntaxError("Unable to parse JSON object");
	c = p.nextClean();
	if(c == '}') return null;
	if(c != '\"' && c != '\'') throw p.syntaxError("Unable to parse JSON object");
	String key = p.nextString(c);
	if(p.nextClean() != ':') throw p.syntaxError("Unable to parse JSON object");
	return key;
}

private static String jsonObjectNext(JSONTokener p) {
	char c = p.nextClean();
	if(c == '}') return null;
	if(c != ',') throw p.syntaxError("Unable to parse JSON object");
	c = p.nextClean();
	if(c != '\"' && c != '\'') throw p.syntaxError("Unable to parse JSON object");
	String key = p.nextString(c);
	if(p.nextClean() != ':') throw p.syntaxError("Unable to parse JSON object");
	return key;
}

}

/**
 * A mutable, internal class representing a Chungamod plugin in the process of being loaded.
 * If the {@code parent} field is not null, properties of the parent loading plugin are inherited in results returned by this class's methods.
 * @author Magnileve
 */
class LoadingPlugin {

final LoadingPlugin parent;
final String id;
String displayName;
String version;
final List<String> moduleClasses = new ArrayList<>();
List<String> mixinFiles;
List<String> dependencies;

LoadingPlugin(LoadingPlugin parent, String id) {
	this.parent = parent;
	this.id = id;
}

String getDisplayName() {
	String str = displayName == null ? (parent == null ? null : parent.getDisplayName()) : displayName;
	return str == null ? id : str;
}

String getVersion() {
	return version == null ? (parent == null ? null : parent.getVersion()) : version;
}

List<String> getModuleClasses() {
	List<String> list = new ArrayList<>(moduleClasses);
	if(parent != null) list.addAll(parent.getModuleClasses());
	return list;
}

List<String> getMixinFiles() {
	List<String> list = new ArrayList<>();
	if(parent != null) list.addAll(parent.getMixinFiles());
	if(mixinFiles != null) list.addAll(mixinFiles);
	return list;
}

List<String> getDependencies() {
	List<String> list = new ArrayList<>();
	if(parent != null) list.addAll(parent.getDependencies());
	if(dependencies != null) list.addAll(dependencies);
	return list;
}

}