package magnileve.chungamod;

/**
 * An error thrown by Chungamod to indicate that a plugin is unable to be loaded.
 * @author Magnileve
 */
public class PluginLoadingError extends Error {

private static final long serialVersionUID = 4826263239961413451L;

/** Constructs a new plugin loading error with {@code null} as its
 * detail message.  The cause is not initialized, and may subsequently be
 * initialized by a call to {@link #initCause}.
 */
public PluginLoadingError() {
    super();
}

/** Constructs a new plugin loading error with the specified detail message.
 * The cause is not initialized, and may subsequently be initialized by a
 * call to {@link #initCause}.
 *
 * @param   message   the detail message. The detail message is saved for
 *          later retrieval by the {@link #getMessage()} method.
 */
public PluginLoadingError(String message) {
    super(message);
}

/**
 * Constructs a new plugin loading error with the specified detail message and
 * cause.  <p>Note that the detail message associated with
 * {@code cause} is <i>not</i> automatically incorporated in
 * this plugin loading error's detail message.
 *
 * @param  message the detail message (which is saved for later retrieval
 *         by the {@link #getMessage()} method).
 * @param  cause the cause (which is saved for later retrieval by the
 *         {@link #getCause()} method).  (A {@code null} value is
 *         permitted, and indicates that the cause is nonexistent or
 *         unknown.)
 */
public PluginLoadingError(String message, Throwable cause) {
    super(message, cause);
}

/** Constructs a new plugin loading error with the specified cause and a
 * detail message of {@code (cause==null ? null : cause.toString())}
 * (which typically contains the class and detail message of
 * {@code cause}).  This constructor is useful for plugin loading errors
 * that are little more than wrappers for other throwables.
 *
 * @param  cause the cause (which is saved for later retrieval by the
 *         {@link #getCause()} method).  (A {@code null} value is
 *         permitted, and indicates that the cause is nonexistent or
 *         unknown.)
 */
public PluginLoadingError(Throwable cause) {
    super(cause);
}

}