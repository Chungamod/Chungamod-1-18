package magnileve.chungamod;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * An error thrown by Chungamod to indicate that not all dependencies of plugins are present.
 * @author Magnileve
 */
public class MissingPluginError extends PluginLoadingError {

private static final long serialVersionUID = -8503351548340674747L;

private final Map<String, List<String>> dependenciesToDependents;

/**
 * Constructions a new missing plugin error with the specified map of dependencies to dependents.
 * A deep copy of the map is created and stored in this error.
 * @param dependenciesToDependents a map of IDs of missing plugins to IDs of plugins that depend on each of them
 */
public MissingPluginError(Map<String, List<String>> dependenciesToDependents) {
	this.dependenciesToDependents = new HashMap<>();
	for(Map.Entry<String, List<String>> entry:dependenciesToDependents.entrySet()) {
		List<String> value = entry.getValue();
		if(!value.isEmpty()) this.dependenciesToDependents.put(entry.getKey(), List.copyOf(value));
	}
}

/**
 * Returns a map of missing plugins to their dependent plugins.
 * @return a map of IDs of missing plugins to IDs of plugins that depend on each of them
 */
public Map<String, List<String>> getMap() {
	return new HashMap<>(dependenciesToDependents);
}

@Override
public String getMessage() {
	StringBuilder str = new StringBuilder("Missing ")
			.append(dependenciesToDependents.size() == 1 ? "plugin: {" : "plugins: {");
	if(dependenciesToDependents.isEmpty()) return str.append('}').toString();
	Iterator<Map.Entry<String, List<String>>> mainIter = dependenciesToDependents.entrySet().iterator();
	while(true) {
		Map.Entry<String, List<String>> entry = mainIter.next();
		Iterator<String> iter = entry.getValue().iterator();
		str.append(entry.getKey()).append(" required by [").append(iter.next());
		while(iter.hasNext()) str.append(", ").append(iter.next());
		if(!mainIter.hasNext()) return str.append(']').toString();
		str.append("], ");
	}
}

}