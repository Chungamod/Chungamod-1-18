package magnileve.chungamod.settings;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import magnileve.chungamod.modules.Module;

/**
 * Identifies a service setting that a parameter should be given the value of.
 * A service setting should be declared like this:
 * <pre>{@code @Setting(name = "...", type = <String.class or String[].class>, limits = "service:<service class name>")}</pre>
 * @author Magnileve
 * @see magnileve.chungamod.Chung#getCallableFactory(Class, magnileve.chungamod.modules.ModuleManager)
 * Chung.getCallableFactory(Class, ModuleManager)
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface GetService {

/**
 * Gets the path of the target setting.
 * @return path of the target setting
 */
String[] value();

/**
 * Gets the type of the module the target setting belongs to.
 * If {@code Module.class} is returned, the module declaring this annotation is used.
 * @return the type of the module the target setting belongs to
 */
Class<? extends Module> moduleType() default Module.class;

/**
 * Indicates if {@code null} values should be given to this parameter,
 * or if {@link UnsetSettingException} should be thrown in such a case.
 * This happens when the value of the service setting is {@code ""}.
 * If the parameter type is wrapped in {@link java.util.Optional Optional}, this method has no affect.
 * @return {@code true} if this parameter is allowed to be set to {@code null}
 */
boolean allowNull() default false;

}