package magnileve.chungamod.events;

/**
 * Posts events of a certain type to an {@link EventManager}.
 * @param <T> the type of event
 * @author Magnileve
 */
@FunctionalInterface
public interface EventPoster<T> {

/**
 * Sends an event to all of its event type's registered listeners.
 * @param event the event
 * @returns {@code true} if this event is cancellable and has been cancelled; {@code false} otherwise
 */
public boolean post(T event);

}