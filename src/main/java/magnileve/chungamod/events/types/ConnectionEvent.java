package magnileve.chungamod.events.types;

import net.minecraft.client.world.ClientWorld;

/**
 * Posted upon joining or leaving a server (includes singleplayer).
 * If joining, the loading {@code ClientWorld} is contained.
 * @author Magnileve
 */
public class ConnectionEvent {

private final ClientWorld world;

/**
 * Creates a new {@code ConnectionEvent} with the loading world if one exists.
 * @param world the loading world, or {@code null} if not applicable
 */
public ConnectionEvent(ClientWorld world) {
	this.world = world;
}

/**
 * Determines if this event is for joining a server.
 * @return {@code true} if this event is for joining; {@code false} if this event is for leaving
 */
public boolean isJoin() {
	return world != null;
}

/**
 * Gets the loading world if this event is a join; {@code null} otherwise.
 * @return the world
 */
public ClientWorld getWorld() {
	return world;
}

@Override
public String toString() {
	return "ConnectionEvent: " + (isJoin() ? "Joining " + getWorld().toString() : "Disconnecting");
}

}