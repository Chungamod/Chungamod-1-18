package magnileve.chungamod.events.types;

import magnileve.chungamod.events.SimpleCancellable;

/**
 * Posted when a chat message is about to be sent to the server.
 * @author Magnileve
 * @see net.minecraft.client.network.ClientPlayerEntity#sendChatMessage(String)
 */
public class ChatMessageEvent extends SimpleCancellable {

private final String message;

/**
 * Creates a new {@code ChatMessageEvent} for the given chat message.
 * @param message the chat message about to be sent
 */
public ChatMessageEvent(String message) {
	this.message = message;
}

/**
 * Returns the chat message about to be sent.
 * @return the chat message about to be sent
 */
public String getMessage() {
	return message;
}

}