package magnileve.chungamod.events.types;

import net.minecraft.client.world.ClientWorld;

/**
 * Posted upon switching worlds without leaving or joining a server (or singleplayer).
 * In vanilla, this is caused by travel through nether or end portals.
 * @author Magnileve
 */
public class SwitchWorldEvent {

private final ClientWorld world;

/**
 * Creates a new {@code SwitchWorldEvent} with the loading world.
 * @param world the loading world
 */
public SwitchWorldEvent(ClientWorld world) {
	this.world = world;
}

/**
 * Gets the loading world.
 * @return the world
 */
public ClientWorld getWorld() {
	return world;
}

@Override
public String toString() {
	return "SwitchWorldEvent: " + "Joining " + getWorld().toString();
}

}