package magnileve.chungamod.events.types;

import java.util.Objects;

import net.minecraft.client.gui.screen.Screen;

/**
 * Posted right before a new GUI screen is initialized and displayed.
 * If a call to {@link #setScreen(Screen)} providing a different screen is made, subsequent events will be posted until no different screen is set.
 * @author Magnileve
 */
public class GuiEvent {

private final Screen initialScreen;
private final boolean isInitialPost;

private Screen screen;

/**
 * Creates a new {@code GuiEvent} with the new screen about to be set.
 * @param screen a new screen
 * @param isInitialPost if this event has not been made in response to a listener changing the screen of a previously posted event
 */
public GuiEvent(Screen screen, boolean isInitialPost) {
	initialScreen = screen;
	this.isInitialPost = isInitialPost;
	this.screen = screen;
	
}

/**
 * Returns the screen about to be set, which could have been changed by another event listener.
 * @return the screen about to be set
 * @see #isScreenReplaced()
 */
public Screen getScreen() {
	return screen;
}

/**
 * Returns the screen about to be set when this event was posted, ignoring any screen provided by a call to {@link #setScreen(Screen)}.
 * @return the screen about to be set when this event was posted
 */
public Screen getInitialScreen() {
	return initialScreen;
}

/**
 * Replaces the screen about to be set.  This will result in a subsequent event being posted.
 * @param screen a new screen
 */
public void setScreen(Screen screen) {
	this.screen = screen;
}

/**
 * Indicates if an event listener has replaced this event's screen.
 * If this method returns {@code true} after an event has been posted, a subsequent event will be posted.
 * @return {@code true} if an event listener has changed this event's screen, {@code false} otherwise
 */
public boolean isScreenReplaced() {
	return !Objects.equals(initialScreen, screen);
}

/**
 * Indicates if this event was not posted as a result of an event listener changing the screen of a directly previously posted event.
 * @return {@code false} if this event was posted as a result of an event listener changing the screen of a directly previously posted event;
 * {@code true} otherwise
 */
public boolean isInitialPost() {
	return isInitialPost;
}

}