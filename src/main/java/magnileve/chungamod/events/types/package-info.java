/**
 * Contains event types supported by Chungamod's event listener instance.
 */
package magnileve.chungamod.events.types;