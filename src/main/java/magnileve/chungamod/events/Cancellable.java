package magnileve.chungamod.events;

/**
 * Indicates that an event is able to be cancelled.
 * All listeners are still notified regardless of cancelled status.
 * @author Magnileve
 */
public interface Cancellable {

/**
 * Sets the cancelled status of this event.
 * @param cancelled {@code true} if this event should be cancelled; {@code false} if this event should not be cancelled
 */
public void setCancelled(boolean cancelled);

/**
 * Indicates whether or not this event is currently set to be cancelled.
 * @return {@code true} if this event is set to be cancelled; {@code false} if not
 */
public boolean isCancelled();

}