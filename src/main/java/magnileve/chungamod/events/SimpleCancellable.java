package magnileve.chungamod.events;

/**
 * A class implementing {@link Cancellable} with no additional function.
 * @author Magnileve
 */
public class SimpleCancellable implements Cancellable {

private boolean cancelled;

/**
 * Creates an new uncancelled {@code Cancellable}.
 */
public SimpleCancellable() {}

/**
 * Creates a new {@code Cancellable}.
 * @param cancelled the initial status to be returned by {@link #isCancelled()}
 */
public SimpleCancellable(boolean cancelled) {
	this.cancelled = cancelled;
}

@Override
public void setCancelled(boolean cancelled) {
	this.cancelled = cancelled;
}

@Override
public boolean isCancelled() {
	return cancelled;
}

}