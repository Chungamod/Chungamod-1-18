package magnileve.chungamod.gui;

/**
 * A button that displays a message when hovered over.
 * @author Magnileve
 */
public abstract class HoverInfoButton extends PotentialInfoButton {

/**
 * Creates a new button.
 * @param id button ID
 * @param x x position
 * @param y y position
 * @param widthIn width
 * @param heightIn height
 * @param name name of button; also the display string unless used differently by a subclass
 * @param rendererFactory factory to build renderer for this button
 * @param messageDisplayer used to display messages
 */
public HoverInfoButton(int id, int x, int y, int widthIn, int heightIn, String name, ButtonRendererFactory<ClickGUIButton> rendererFactory,
		UpdatableDisplay messageDisplayer) {
	super(id, x, y, widthIn, heightIn, name, rendererFactory, messageDisplayer);
}

/**
 * Gets a message to be displayed when this button is hovered.
 * @return if this button's name is trimmed, the name and a newline; if not, an empty string
 */
protected String getHoverMessage() {
	return isNameTrimmed() ? getName() + '\n' : "";
}

@Override
protected void onHover() {
	if(isHovered()) displayMessage(getHoverMessage());
	else hideDisplayedMessage();
}

}