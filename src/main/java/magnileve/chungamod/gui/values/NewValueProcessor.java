package magnileve.chungamod.gui.values;

import magnileve.chungamod.util.json.InvalidValueException;

/**
 * Operates on and verifies new values.
 * @author Magnileve
 * @param <T> value type
 * @see ValueProcessor
 */
@FunctionalInterface
public interface NewValueProcessor<T> {

/**
 * Accepts an input value.
 * @param newValue a value
 * @return the value after operated on
 * @throws InvalidValueException if the given value is invalid
 */
public T processNewValue(T newValue) throws InvalidValueException;

}