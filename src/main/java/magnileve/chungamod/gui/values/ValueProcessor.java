package magnileve.chungamod.gui.values;

import magnileve.chungamod.util.json.InvalidValueException;

/**
 * Operates on and verifies values.
 * @author Magnileve
 * @param <T> value type
 */
public interface ValueProcessor<T> extends NewValueProcessor<T>, DefaultValueProcessor<T> {

/**
 * A {@code ValueProcessor} backed by functional interfaces.
 * @author Magnileve
 * @param <T> value type
 */
public class Of<T> implements ValueProcessor<T> {
	private final NewValueProcessor<T> processValue;
	private final DefaultValueProcessor<T> resetValue;
	
	public Of(NewValueProcessor<T> processValue, DefaultValueProcessor<T> processDefaultValue) {
		this.processValue = processValue;
		this.resetValue = processDefaultValue;
		
	}
	
	@Override
	public T processNewValue(T newValue) throws InvalidValueException {
		return processValue.processNewValue(newValue);
	}
	
	@Override
	public T processDefaultValue() throws InvalidValueException {
		return resetValue.processDefaultValue();
	}
}

}