package magnileve.chungamod.gui.values;

import magnileve.chungamod.util.json.InvalidValueException;

/**
 * Operates on and verifies a default value.
 * @author Magnileve
 * @param <T> value type
 * @see ValueProcessor
 */
@FunctionalInterface
public interface DefaultValueProcessor<T> {

/**
 * Accepts the default value.
 * @return the value after operated on
 * @throws InvalidValueException if the default value is invalid or cannot be obtained
 */
public T processDefaultValue() throws InvalidValueException;

}