package magnileve.chungamod.gui;

/**
 * A {@code MouseHandler} handles callbacks related to mouse actions.
 * @author Magnileve
 */
public interface MouseHandler {

/**
 * Callback for when a mouse move event has been captured.
 * 
 * @param mouseX the X coordinate of the mouse
 * @param mouseY the Y coordinate of the mouse
 */
public void mouseMoved(double mouseX, double mouseY);

/**
 * Callback for when a mouse button down event
 * has been captured.
 * 
 * The button number is identified by the constants in
 * {@link org.lwjgl.glfw.GLFW GLFW} class.
 * 
 * @param mouseY the Y coordinate of the mouse
 * @param mouseX the X coordinate of the mouse
 * @param mouseButton the mouse button number
 */
public void mouseClicked(double mouseX, double mouseY, int mouseButton);

/**
 * Callback for when a mouse button release event
 * has been captured.
 * 
 * The button number is identified by the constants in
 * {@link org.lwjgl.glfw.GLFW GLFW} class.
 * 
 * @param mouseY the Y coordinate of the mouse
 * @param mouseX the X coordinate of the mouse
 * @param mouseButton the mouse button number
 */
public void mouseReleased(double mouseX, double mouseY, int mouseButton);

/**
 * Callback for when a mouse button scroll event
 * has been captured.
 * 
 * @param mouseX the X coordinate of the mouse
 * @param mouseY the Y coordinate of the mouse
 * @param up {@code true} if scrolled up; {@code false} if scrolled down
 */
public void mouseScrolled(double mouseX, double mouseY, boolean up);

}