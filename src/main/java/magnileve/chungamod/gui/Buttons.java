package magnileve.chungamod.gui;

import static magnileve.chungamod.gui.InheritanceButtonRendererFactory.putRF;
import static magnileve.chungamod.gui.InheritanceButtonRendererFactory.putST;
import static magnileve.chungamod.gui.values.ValueButtonFactoryMap.putVF;

import java.awt.Color;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;
import java.util.function.Function;
import java.util.function.IntConsumer;
import java.util.function.IntSupplier;

import com.mojang.blaze3d.systems.RenderSystem;

import magnileve.chungamod.gui.values.ArrayButton;
import magnileve.chungamod.gui.values.BlockPosButton;
import magnileve.chungamod.gui.values.BooleanButton;
import magnileve.chungamod.gui.values.ColorButton;
import magnileve.chungamod.gui.values.ColorPicker;
import magnileve.chungamod.gui.values.ColorComponentButton;
import magnileve.chungamod.gui.values.DecimalRangeButton;
import magnileve.chungamod.gui.values.EnumButton;
import magnileve.chungamod.gui.values.IntRangeButton;
import magnileve.chungamod.gui.values.JSONButton;
import magnileve.chungamod.gui.values.RangeButton;
import magnileve.chungamod.gui.values.StringButton;
import magnileve.chungamod.gui.values.ValueButton;
import magnileve.chungamod.gui.values.ValueButtonFactory;
import magnileve.chungamod.gui.values.ValueButtonFactoryLink;
import magnileve.chungamod.gui.values.ValueButtonFactoryMap;
import magnileve.chungamod.gui.values.ValueButtonTypeFactory;
import magnileve.chungamod.gui.values.ValueProcessor;
import magnileve.chungamod.util.Bucket;
import magnileve.chungamod.util.ClassHashMap;
import magnileve.chungamod.util.Corner;
import magnileve.chungamod.util.function.TriConsumer;
import magnileve.chungamod.util.json.JSONManager;
import magnileve.chungamod.util.json.JSONUtil;
import magnileve.chungamod.util.math.Vec2i;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.render.BufferBuilder;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.render.Tessellator;
import net.minecraft.client.render.VertexFormat;
import net.minecraft.client.render.VertexFormats;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Matrix4f;
import net.minecraft.util.math.Vec3i;

/**
 * Contains utility methods for buttons, mostly to build provided implementations of button factories.
 * @author Magnileve
 */
public class Buttons {

private Buttons() {}

/**
 * Creates a {@link ValueButtonFactory} from a {@link ValueButtonFactoryLink}.
 * @param factory internal factory
 * @param clickGUI GUI of this factory
 * @param buttonIDSupplier generates button IDs
 * @param json manages JSON
 * @param rendererFactory builds renderers for buttons
 * @return a new {@link ValueButtonFactory}
 */
public static ValueButtonFactory completeValueButtonFactory(ValueButtonFactoryLink factory,
		ClickGUI clickGUI, IntSupplier buttonIDSupplier, JSONManager json, ButtonRendererFactory<ClickGUIButton> rendererFactory) {
	return new ValueButtonFactory() {
		@Override
		public <T> ValueButton<T> build(ValueButtonFactory factory1, int id, int x, int y, int widthIn, int heightIn,
				String name, T value, ValueProcessor<T> valueProcessor,
				Function<ValueButton<T>, MenuButtonBuilder> menuButtonBuilder, MenuChain menuChain, String description,
				Class<T> type, boolean allowNull, String limits) {
			return factory.build(factory1, id, x, y, widthIn, heightIn, name,
					value, valueProcessor, menuButtonBuilder, menuChain, description, type, allowNull, limits);
		}
		@Override public ClickGUI getClickGUI() {return clickGUI;}
		@Override public IntSupplier getButtonIDSupplier() {return buttonIDSupplier;}
		@Override public JSONManager getJSONManager() {return json;}
		@Override public ButtonRendererFactory<ClickGUIButton> getRendererFactory() {return rendererFactory;}
	};
}

/**
 * Builds the default {@link ValueButtonFactory}.
 * @param clickGUI GUI of this factory
 * @param buttonIDSupplier generates button IDs
 * @param json manages JSON
 * @param rendererFactory builds renderers for buttons
 * @return a new {@link ValueButtonFactory}
 */
public static ValueButtonFactory defaultValueButtonFactory(ClickGUI clickGUI,
		IntSupplier buttonIDSupplier, JSONManager json, ButtonRendererFactory<ClickGUIButton> rendererFactory) {
	return completeValueButtonFactory(new ValueButtonFactoryMap(DEFAULT_VALUE_BUTTON_FACTORIES, JSON_FACTORY, ARRAY_FACTORY, ENUM_FACTORY),
			clickGUI, buttonIDSupplier, json, rendererFactory);
}

/**
 * A {@link ValueButtonFactoryLink} that creates a {@link JSONButton} from any type.
 */
public static final ValueButtonFactoryLink JSON_FACTORY = new ValueButtonFactoryLink() {
	@SuppressWarnings("resource")
	@Override
	public <T> ValueButton<T> build(ValueButtonFactory factory, int id, int x, int y, int widthIn, int heightIn, String name,
			T value, ValueProcessor<T> valueProcessor, Function<ValueButton<T>, MenuButtonBuilder> menuButtonBuilder,
			MenuChain menuChain, String description, Class<T> type, boolean allowNull, String limits) {
		ClickGUI clickGUI = factory.getClickGUI();
		return new JSONButton<T>(id, x, y, widthIn, heightIn, name, factory.getRendererFactory(), clickGUI.getDisplayer(),
				value, valueProcessor, menuButtonBuilder, menuChain, factory.getButtonIDSupplier(), description,
				clickGUI.getKeyboardPermit(), clickGUI.getMinecraft().keyboard, type, factory.getJSONManager(), allowNull);
	}
};

/**
 * A {@link ValueButtonFactoryLink} that creates an {@link ArrayButton} from any array type extending {@code Object[]}.
 * If a non-array type is passed into this factory, it returns {@code null}.
 */
public static final ValueButtonFactoryLink ARRAY_FACTORY = new ValueButtonFactoryLink() {
	@SuppressWarnings("unchecked")
	@Override
	public <T> ValueButton<T> build(ValueButtonFactory factory, int id, int x, int y, int widthIn, int heightIn, String name,
			T value, ValueProcessor<T> valueProcessor, Function<ValueButton<T>, MenuButtonBuilder> menuButtonBuilder,
			MenuChain menuChain, String description, Class<T> type, boolean allowNull, String limits) {
		return type.isArray() ? (ValueButton<T>) makeArrayButton(factory, id, x, y, widthIn, heightIn, name,
				value, valueProcessor, menuButtonBuilder, menuChain, description, type.getComponentType(), allowNull, limits) : null;
	}
	
	@SuppressWarnings("unchecked")
	private <C> ArrayButton<C> makeArrayButton(ValueButtonFactory factory, int id, int x, int y, int widthIn, int heightIn,
			String name, Object value, ValueProcessor<?> valueProcessor, Function<?, ?> menuButtonBuilder,
			MenuChain menuChain, String description, Class<C> componentType, boolean allowNull, String limits) {
		ClickGUI clickGUI = factory.getClickGUI();
		return new ArrayButton<>(id, x, y, widthIn, heightIn, name, factory.getRendererFactory(), clickGUI.getDisplayer(),
				(C[]) value, (ValueProcessor<C[]>) valueProcessor,
				(Function<ValueButton<C[]>, MenuButtonBuilder>) menuButtonBuilder, menuChain, factory.getButtonIDSupplier(),
				description, componentType, clickGUI, factory.getJSONManager(), limits, factory);
	}
};

/**
 * A {@link ValueButtonFactoryLink} that creates an {@link EnumButton} from any {@code enum} type.
 * If a non-enum type is passed into this factory, it returns {@code null}.
 */
public static final ValueButtonFactoryLink ENUM_FACTORY = new ValueButtonFactoryLink() {
	@Override
	public <T> ValueButton<T> build(ValueButtonFactory factory, int id, int x, int y, int widthIn, int heightIn, String name,
			T value, ValueProcessor<T> valueProcessor, Function<ValueButton<T>, MenuButtonBuilder> menuButtonBuilder,
			MenuChain menuChain, String description, Class<T> type, boolean allowNull, String limits) {
		ClickGUI clickGUI = factory.getClickGUI();
		return type.isEnum() ? new EnumButton<>(id, x, y, widthIn, heightIn, name, factory.getRendererFactory(),
				clickGUI.getDisplayer(), value, valueProcessor, menuButtonBuilder, menuChain,
				factory.getButtonIDSupplier(), description, clickGUI.getMousePermit(), type.getEnumConstants()) : null;
	}
};

/**
 * Maps types to default factories for value buttons containing those types.
 */
public static final Map<Class<?>, ValueButtonTypeFactory<?>> DEFAULT_VALUE_BUTTON_FACTORIES;

static {
	Map<Class<?>, ValueButtonTypeFactory<?>> map = new ClassHashMap<>();
	DEFAULT_VALUE_BUTTON_FACTORIES = Collections.unmodifiableMap(map);
	clinit(map);
}

@SuppressWarnings("resource")
private static final void clinit(Map<Class<?>, ValueButtonTypeFactory<?>> map) {
	putVF(map, Boolean.class, (factory, id, x, y, widthIn, heightIn, name,
			value, valueProcessor, menuButtonBuilder, menuChain, description, type, allowNull, limits) ->
			new BooleanButton(id, x, y, widthIn, heightIn, name, factory.getRendererFactory(), factory.getClickGUI().getDisplayer(),
					value, valueProcessor, menuButtonBuilder, menuChain, factory.getButtonIDSupplier(), description));
	putVF(map, String.class, (factory, id, x, y, widthIn, heightIn, name,
			value, valueProcessor, menuButtonBuilder, menuChain, description, type, allowNull, limits) -> {
				String[] limitArray = JSONUtil.parseLimits(limits);
				ClickGUI clickGUI = factory.getClickGUI();
				
				if(limitArray[0].equals("values")) return new EnumButton<>(id, x, y, widthIn, heightIn, name,
						factory.getRendererFactory(), clickGUI.getDisplayer(), value, valueProcessor, menuButtonBuilder,
						menuChain, factory.getButtonIDSupplier(), description, clickGUI.getMousePermit(), limitArray[1].split(","));
				
				return new StringButton(id, x, y, widthIn, heightIn, name, factory.getRendererFactory(), clickGUI.getDisplayer(),
						value, valueProcessor, menuButtonBuilder, menuChain, factory.getButtonIDSupplier(),
						description, clickGUI.getKeyboardPermit(), clickGUI.getMinecraft().keyboard);
			});
	putVF(map, Integer.class, (factory, id, x, y, widthIn, heightIn, name,
			value, valueProcessor, menuButtonBuilder, menuChain, description, type, allowNull, limits) -> {
				Vec2i range = IntRangeButton.parseRange(limits);
				ClickGUI clickGUI = factory.getClickGUI();
				return range == null ? null : new IntRangeButton.IntegerButton(id, x, y, widthIn, heightIn, name,
						factory.getRendererFactory(), clickGUI.getDisplayer(), value, valueProcessor, menuButtonBuilder,
						menuChain, factory.getButtonIDSupplier(), description, clickGUI.getMousePermit(), range.getX(), range.getY());
			});
	putVF(map, Byte.class, (factory, id, x, y, widthIn, heightIn, name,
			value, valueProcessor, menuButtonBuilder, menuChain, description, type, allowNull, limits) -> {
				Vec2i range = IntRangeButton.parseRange(limits);
				ClickGUI clickGUI = factory.getClickGUI();
				return range == null ? null : new IntRangeButton.ByteButton(id, x, y, widthIn, heightIn, name,
						factory.getRendererFactory(), clickGUI.getDisplayer(), value, valueProcessor, menuButtonBuilder,
						menuChain, factory.getButtonIDSupplier(), description, clickGUI.getMousePermit(), range.getX(), range.getY());
			});
	putVF(map, Short.class, (factory, id, x, y, widthIn, heightIn, name,
			value, valueProcessor, menuButtonBuilder, menuChain, description, type, allowNull, limits) -> {
				Vec2i range = IntRangeButton.parseRange(limits);
				ClickGUI clickGUI = factory.getClickGUI();
				return range == null ? null : new IntRangeButton.ShortButton(id, x, y, widthIn, heightIn, name,
						factory.getRendererFactory(), clickGUI.getDisplayer(), value, valueProcessor, menuButtonBuilder,
						menuChain, factory.getButtonIDSupplier(), description, clickGUI.getMousePermit(), range.getX(), range.getY());
			});
	putVF(map, BigDecimal.class, (factory, id, x, y, widthIn, heightIn, name,
			value, valueProcessor, menuButtonBuilder, menuChain, description, type, allowNull, limits) -> {
				Bucket<BigDecimal, BigDecimal> range = DecimalRangeButton.parseRange(limits);
				if(range == null) return null;
				ClickGUI clickGUI = factory.getClickGUI();
				return new DecimalRangeButton(id, x, y, widthIn, heightIn, name, factory.getRendererFactory(), clickGUI.getDisplayer(),
						value, valueProcessor, menuButtonBuilder, menuChain, factory.getButtonIDSupplier(),
						description, clickGUI.getMousePermit(), range.getE1(), range.getE2());
			});
	putVF(map, BlockPos.class, (factory, id, x, y, widthIn, heightIn, name,
			value, valueProcessor, menuButtonBuilder, menuChain, description, type, allowNull, limits) -> {
				ClickGUI clickGUI = factory.getClickGUI();
				return new BlockPosButton(id, x, y, widthIn, heightIn, name, factory.getRendererFactory(), clickGUI.getDisplayer(),
						value, valueProcessor, menuButtonBuilder, menuChain, factory.getButtonIDSupplier(),
						description, clickGUI.getKeyboardPermit(), clickGUI.getMinecraft().keyboard);
			});
	putVF(map, Color.class, (factory, id, x, y, widthIn, heightIn, name,
			value, valueProcessor, menuButtonBuilder, menuChain, description, type, allowNull, limits) -> {
				ClickGUI clickGUI = factory.getClickGUI();
				return new ColorButton(id, x, y, widthIn, heightIn, name,
						factory.getRendererFactory(), clickGUI.getDisplayer(), value, valueProcessor, menuButtonBuilder,
						menuChain, factory.getButtonIDSupplier(), description, clickGUI.getKeyboardPermit(), clickGUI.getMinecraft().keyboard,
						factory.getJSONManager(), clickGUI.getMousePermit());
			});
}

/**
 * Returns a consumer that renders a rectangle.
 * Be sure that {@link ClickGUIButtonImpl#preButtonRender()} is called before calling the returned consumer.
 * @param x first x position
 * @param y first y position
 * @param x2 second x position
 * @param y2 second y position
 * @param zLevel z position
 * @param redfill red color component
 * @param greenfill green color component
 * @param bluefill blue color component
 * @param alphafill alpha color component
 * @return a consumer that renders a rectangle
 */
public static TriConsumer<Tessellator, BufferBuilder, Matrix4f> drawRect(int x, int y, int x2, int y2,
		float zLevel, int redfill, int greenfill, int bluefill, int alphafill) {
	return (tessellator, buffer, m) -> {
		RenderSystem.setShader(GameRenderer::getPositionColorShader);
		buffer.begin(VertexFormat.DrawMode.QUADS, VertexFormats.POSITION_COLOR);
		//draw button
		buffer.vertex(m, x2, y, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, x, y, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, x, y2, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, x2, y2, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		tessellator.draw();
	};
}

/**
 * Returns a consumer that renders the borders of a rectangle.
 * Be sure that {@link ClickGUIButtonImpl#preButtonRender()} is called before calling the returned consumer.
 * @param x first x position
 * @param y first y position
 * @param x2 second x position
 * @param y2 second y position
 * @param borderWidth width of borders
 * @param zLevel z position
 * @param redfill red color component
 * @param greenfill green color component
 * @param bluefill blue color component
 * @param alphafill alpha color component
 * @return a consumer that renders the borders of a rectangle
 */
public static TriConsumer<Tessellator, BufferBuilder, Matrix4f> drawBorders(int x, int y, int x2, int y2, float borderWidth,
		float zLevel, int redfill, int greenfill, int bluefill, int alphafill) {
	if(borderWidth == 0D) return (tessellator, buffer, m) -> {};
	float x1 = x - borderWidth, y1 = y - borderWidth, x3 = x2 + borderWidth, y3 = y2 + borderWidth;
	return (tessellator, buffer, m) -> {
		RenderSystem.setShader(GameRenderer::getPositionColorShader);
		buffer.begin(VertexFormat.DrawMode.QUADS, VertexFormats.POSITION_COLOR);
		//draw top
		buffer.vertex(m, x2, y1, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, x1, y1, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, x1, y, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, x2, y, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		//draw left
		buffer.vertex(m, x, y, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, x1, y, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, x1, y3, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, x, y3, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		//draw bottom
		buffer.vertex(m, x3, y2, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, x, y2, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, x, y3, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, x3, y3, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		//draw right
		buffer.vertex(m, x3, y1, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, x2, y1, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, x2, y2, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, x3, y2, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		tessellator.draw();
	};
}

/**
 * Returns a consumer that renders the visible borders of a rectangle in a corner.
 * Be sure that {@link ClickGUIButtonImpl#preButtonRender()} is called before calling the returned consumer.
 * @param corner the corner of the rectangle
 * @param x left x position
 * @param y top y position
 * @param x2 right x position
 * @param y2 bottom y position
 * @param borderWidth width of borders
 * @param zLevel z position
 * @param redfill red color component
 * @param greenfill green color component
 * @param bluefill blue color component
 * @param alphafill alpha color component
 * @return a consumer that renders the visible borders of a rectangle
 */
public static TriConsumer<Tessellator, BufferBuilder, Matrix4f> drawVisibleBorders(Corner corner, int x, int y, int x2, int y2, float borderWidth,
		float zLevel, int redfill, int greenfill, int bluefill, int alphafill) {
	if(borderWidth == 0D) return (tessellator, buffer, m) -> {};
	float xH, xH2, xV, xV2, yH, yH2, yV = y, yV2 = y2;
	if(corner.isLeft()) {
		xH = x;
		xH2 = x2 + borderWidth;
		xV = x2;
		xV2 = xH2;
	} else {
		xH = x - borderWidth;
		xH2 = x2;
		xV = xH;
		xV2 = x;
	}
	if(corner.isTop()) {
		yH = y2;
		yH2 = y2 + borderWidth;
	} else {
		yH = y - borderWidth;
		yH2 = y;
	}
	return (tessellator, buffer, m) -> {
		RenderSystem.setShader(GameRenderer::getPositionColorShader);
		buffer.begin(VertexFormat.DrawMode.QUADS, VertexFormats.POSITION_COLOR);
		//draw horizontal
		buffer.vertex(m, xH2, yH, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, xH, yH, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, xH, yH2, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, xH2, yH2, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		//draw vertical
		buffer.vertex(m, xV2, yV, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, xV, yV, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, xV, yV2, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		buffer.vertex(m, xV2, yV2, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
		tessellator.draw();
	};
}

/**
 * Builds a {@link ButtonRenderer} factory for a basic {@link ClickGUIButton}.
 * @param fontRenderer font renderer
 * @param prop properties of buttons
 * @return a {@link Function} that takes in a {@code ClickGUIButton} and produces a {@code ButtonRenderer}
 */
public static Function<ClickGUIButton, ButtonRenderer> simpleRenderer(TextRenderer fontRenderer, ButtonProperties prop) {
	float zLevel = 0F;
	return b -> {
		int redfill, greenfill, bluefill;
		if(b.isHovered()) {
			redfill = prop.brighter(prop.redfill);
			greenfill = prop.brighter(prop.greenfill);
			bluefill = prop.brighter(prop.bluefill);
		} else {
			redfill = prop.redfill;
			greenfill = prop.greenfill;
			bluefill = prop.bluefill;
		}
		float bWidth = prop.buttonBorderWidth;
		int bWidth1 = (int) bWidth, bWidth2 = bWidth1 / 2, x = b.getX() + bWidth2, y = b.getY() + bWidth2,
				width = b.getWidth(), height = b.getHeight(), x2 = x + width - bWidth1, y2 = y + height - bWidth1;
		String displayString = b.getDisplayString();
		float textX = x + (width - fontRenderer.getWidth(displayString)) / 2,
				textY = y + (height - fontRenderer.fontHeight) / 2 + 1;
		TriConsumer<Tessellator, BufferBuilder, Matrix4f> drawRect = drawRect(x, y, x2, y2, zLevel, redfill, greenfill, bluefill, prop.alphafill),
				drawBorders = drawBorders(x, y, x2, y2, bWidth, zLevel, prop.redborder, prop.greenborder, prop.blueborder, prop.alphaborder);
		
		return new ButtonRenderer() {
			@Override
			public void drawButton(Tessellator tessellator, BufferBuilder buffer, MatrixStack matrices) {
				Matrix4f matrix = matrices.peek().getPositionMatrix();
				drawRect.accept(tessellator, buffer, matrix);
				drawBorders.accept(tessellator, buffer, matrix);
			}
			
			@Override
			public void drawText(MatrixStack matrices) {
				fontRenderer.draw(matrices, displayString, textX, textY, prop.buttonTextColor);
			}
		};
	};
}

/**
 * Builds the default {@link ButtonRendererFactory}.
 * @param fontRenderer font renderer
 * @param prop properties of buttons
 * @return a new {@link ButtonRendererFactory} containing all default renderer factories and string trimmers.
 */
public static ButtonRendererFactory<ClickGUIButton> rendererFactory(TextRenderer fontRenderer, ButtonProperties prop) {
	Map<Class<? extends ClickGUIButton>, Class<? extends ClickGUIButton>> priorityInheritance =
			new ClassHashMap<>(4);
	return new InheritanceButtonRendererFactory<>(rendererFactoryMap(fontRenderer, prop, priorityInheritance, new ClassHashMap<>()),
			priorityInheritance, new ClassHashMap<>(64, 0.5F),
			stringTrimmerMap(fontRenderer, prop, priorityInheritance, new ClassHashMap<>()), new ClassHashMap<>(8, 0.5F));
}

/**
 * Adds all default {@link ButtonRenderer} factories to a map.
 * @param fontRenderer font renderer
 * @param prop properties of buttons
 * @param priorityInheritance map to add prioritized inherited types to
 * @param map map to add factories to
 * @return {@code map}
 */
public static Map<Class<? extends ClickGUIButton>, Function<? extends ClickGUIButton, ButtonRenderer>> rendererFactoryMap(
		TextRenderer fontRenderer, ButtonProperties prop,
		Map<Class<? extends ClickGUIButton>, Class<? extends ClickGUIButton>> priorityInheritance,
		Map<Class<? extends ClickGUIButton>, Function<? extends ClickGUIButton, ButtonRenderer>> map) {
	float zLevel = 0F;
	
	putRF(map, ClickGUIButton.class, simpleRenderer(fontRenderer, prop));
	putRF(map, DisplayButton.class, b -> {
		float bWidth = prop.buttonBorderWidth;
		int bWidth1 = (int) bWidth, bWidth2 = bWidth1 / 2, x = b.getX() + bWidth2, y = b.getY() + bWidth2,
				width = b.getWidth(), height = b.getHeight(), x2 = x + width - bWidth1, y2 = y + height - bWidth1;
		String displayString = b.getDisplayString();
		float textX = x + (width - fontRenderer.getWidth(displayString)) / 2,
				textY = y + (height - fontRenderer.fontHeight) / 2 + 1;
		TriConsumer<Tessellator, BufferBuilder, Matrix4f>
				drawRect = drawRect(x, y, x2, y2, zLevel, prop.redfill, prop.greenfill, prop.bluefill, prop.alphafill),
				drawBorders = drawBorders(x, y, x2, y2, bWidth, zLevel, prop.redborder, prop.greenborder, prop.blueborder, prop.alphaborder);
		
		return new ButtonRenderer() {
			@Override
			public void drawButton(Tessellator tessellator, BufferBuilder buffer, MatrixStack matrices) {
				Matrix4f matrix = matrices.peek().getPositionMatrix();
				drawRect.accept(tessellator, buffer, matrix);
				drawBorders.accept(tessellator, buffer, matrix);
			}
			
			@Override
			public void drawText(MatrixStack matrices) {
				fontRenderer.draw(matrices, displayString, textX, textY, prop.buttonTextColor);
			}
		};
	});
	putRF(map, Menu.class, b -> {
		float bWidth = prop.buttonBorderWidth;
		int bWidth1 = (int) bWidth, bWidth2 = bWidth1 / 2, x = b.getX() + bWidth2, y = b.getY() + bWidth2,
				height = b.getHeight(), y2 = y + height - bWidth1;
		if(b.isBeingScrolled()) {
			int padding = b.getDividerSize(), x2 = x + (padding < 2 ? b.getWidth() + 2 - padding : b.getWidth()) - bWidth1,
					redfill1 = prop.brighter(prop.redfill), greenfill1 = prop.brighter(prop.greenfill), bluefill1 = prop.brighter(prop.bluefill),
					scrollBarX = x2 - (padding < 2 ? 2 : padding), scrollableHeight = b.getScrollableHeight(),
					scrollBarY = y + b.getScrollHeight() * height / scrollableHeight,
					scrollBarY2 = scrollBarY + (height * height - 1) / scrollableHeight + 1;
			TriConsumer<Tessellator, BufferBuilder, Matrix4f>
					drawRect = drawRect(x, y, x2, y2, zLevel, prop.redfill, prop.greenfill, prop.bluefill, prop.alphafill),
					drawScroll = drawRect(scrollBarX, scrollBarY, x2, scrollBarY2, zLevel,
							redfill1, greenfill1, bluefill1, prop.alphafill),
					drawBorders = drawBorders(x, y, x2, y2, bWidth, zLevel,
							prop.redborder, prop.greenborder, prop.blueborder, prop.alphaborder);
			
			return new ButtonRenderer() {
				@Override
				public void drawButton(Tessellator tessellator, BufferBuilder buffer, MatrixStack matrices) {
					Matrix4f matrix = matrices.peek().getPositionMatrix();
					drawRect.accept(tessellator, buffer, matrix);
					drawScroll.accept(tessellator, buffer, matrix);
					drawBorders.accept(tessellator, buffer, matrix);
				}
				
				@Override public void drawText(MatrixStack matrices) {}
			};
		} else {
			int x2 = x + b.getWidth() - bWidth1;
			TriConsumer<Tessellator, BufferBuilder, Matrix4f>
					drawRect = drawRect(x, y, x2, y2, zLevel, prop.redfill, prop.greenfill, prop.bluefill, prop.alphafill),
					drawBorders = drawBorders(x, y, x2, y2, bWidth, zLevel,
							prop.redborder, prop.greenborder, prop.blueborder, prop.alphaborder);
			return new ButtonRenderer() {
				@Override
				public void drawButton(Tessellator tessellator, BufferBuilder buffer, MatrixStack matrices) {
					Matrix4f matrix = matrices.peek().getPositionMatrix();
					drawRect.accept(tessellator, buffer, matrix);
					drawBorders.accept(tessellator, buffer, matrix);
				}
				
				@Override public void drawText(MatrixStack matrices) {}
			};
		}
	});
	priorityInheritance.put(MenuImpl.class, Menu.class);
	putRF(map, MenuImpl.Header.class, b -> {
		float textX = b.getX() + (b.getWidth() - fontRenderer.getWidth(b.getDisplayString())) / 2,
				textY = b.getY() + (b.getHeight() - fontRenderer.fontHeight) / 2 + 1;
		String displayString = b.getDisplayString();
		
		return new ButtonRenderer() {
			@Override
			public void drawButton(Tessellator tessellator, BufferBuilder buffer, MatrixStack matrices) {}
			
			@Override
			public void drawText(MatrixStack matrices) {
				fontRenderer.draw(matrices, displayString, textX, textY, prop.buttonTextColor);
			}
		};
	});
	putRF(map, UpdatableDisplayButton.class, b -> {
		return b.isVisible() ? null : ButtonRenderer.BLANK;
	});
	priorityInheritance.put(UpdatableDisplayButtonImpl.class, UpdatableDisplayButton.class);
	putRF(map, ResizableDisplayButton.class, b -> {
		float bWidth = prop.buttonBorderWidth;
		if(!b.isVisible()) return ButtonRenderer.BLANK;
		int bWidth1 = (int) bWidth, bWidth2 = bWidth1 / 2, x = b.getX() + bWidth2, y = b.getY() + bWidth2,
				x2 = x + b.getWidth() - bWidth1, y2 = y + b.getHeight() - bWidth1;
		TriConsumer<Tessellator, BufferBuilder, Matrix4f>
				drawRect = drawRect(x, y, x2, y2, zLevel, prop.redfill, prop.greenfill, prop.bluefill, prop.alphafill),
				drawBorders = drawBorders(x, y, x2, y2, bWidth, zLevel,
						prop.redborder, prop.greenborder, prop.blueborder, prop.alphaborder);
		String[] displayLines = b.getDisplayLines().toArray(new String[b.getDisplayLines().size()]);
		
		return new ButtonRenderer() {
			@Override
			public void drawButton(Tessellator tessellator, BufferBuilder buffer, MatrixStack matrices) {
				Matrix4f matrix = matrices.peek().getPositionMatrix();
				drawRect.accept(tessellator, buffer, matrix);
				drawBorders.accept(tessellator, buffer, matrix);
			}
			
			@Override
			public void drawText(MatrixStack matrices) {
				int lineY = y + (b.getLineHeight() - fontRenderer.fontHeight) / 2 + 1;
				for(String line:displayLines) {
					fontRenderer.draw(matrices, line, x, lineY, prop.buttonTextColor);
					lineY += b.getLineHeight();
				}
			}
		};
	});
	putRF(map, CornerDisplayButton.class, b -> {
		if(b.isVisible()) {
			if(b.getY() == -1) return null;
			Corner corner = b.getCorner();
			int bWidth2 = (int) prop.buttonBorderWidth / 2,
					x = corner.isLeft() ? b.getX() : b.getX() + bWidth2, height = b.getHeight() - bWidth2, x2 = x + b.getWidth() - bWidth2,
					lineHeight = b.getLineHeight();
			String[] displayLines = b.getDisplayLines().toArray(new String[b.getDisplayLines().size()]);
			
			class CornerButtonRenderer implements ButtonRenderer, IntConsumer {
				int y, y2;
				TriConsumer<Tessellator, BufferBuilder, Matrix4f> drawRect, drawBorders;
				
				@Override
				public void drawButton(Tessellator tessellator, BufferBuilder buffer, MatrixStack matrices) {
					Matrix4f matrix = matrices.peek().getPositionMatrix();
					drawRect.accept(tessellator, buffer, matrix);
					drawBorders.accept(tessellator, buffer, matrix);
				}
				
				@Override
				public void drawText(MatrixStack matrices) {
					int lineY = y + (lineHeight - fontRenderer.fontHeight) / 2 + 1;
					for(String line:displayLines) {
						fontRenderer.draw(matrices, line, x, lineY, prop.buttonTextColor);
						lineY += lineHeight;
					}
				}
				
				@Override
				public void accept(int value) {
					y = corner.isTop() ? value : value + bWidth2;
					y2 = y + height;
					
					drawRect = drawRect(x, y, x2, y2, zLevel, prop.redfill, prop.greenfill, prop.bluefill, prop.alphafill);
					drawBorders = drawVisibleBorders(corner, x, y, x2, y2, prop.buttonBorderWidth, zLevel,
							prop.redborder, prop.greenborder, prop.blueborder, prop.alphaborder);
				}
			}
			
			CornerButtonRenderer renderer = new CornerButtonRenderer();
			renderer.accept(b.getY());
			b.setYValueConsumer(renderer);
			return renderer;
		} else {
			b.setYValueConsumer(null);
			return ButtonRenderer.BLANK;
		}
	});
	putRF(map, ConfigButton.class, b -> {
		int redfill, greenfill, bluefill;
		if(b.isEnabled() || b.isHovered()) {
			redfill = prop.brighter(prop.redfill);
			greenfill = prop.brighter(prop.greenfill);
			bluefill = prop.brighter(prop.bluefill);
		} else {
			redfill = prop.redfill;
			greenfill = prop.greenfill;
			bluefill = prop.bluefill;
		}
		float bWidth = prop.buttonBorderWidth;
		int bWidth1 = (int) bWidth, bWidth2 = bWidth1 / 2, x = b.getX() + bWidth2, y = b.getY() + bWidth2,
				width = b.getWidth(), height = b.getHeight(), x2 = x + width - bWidth1, y2 = y + height - bWidth1;
		String displayString = b.getDisplayString();
		float textX = x + (width - fontRenderer.getWidth(displayString)) / 2,
				textY = y + (height - fontRenderer.fontHeight) / 2 + 1;
		TriConsumer<Tessellator, BufferBuilder, Matrix4f> drawRect = drawRect(x, y, x2, y2, zLevel, redfill, greenfill, bluefill, prop.alphafill),
				drawBorders = drawBorders(x, y, x2, y2, bWidth, zLevel, prop.redborder, prop.greenborder, prop.blueborder, prop.alphaborder);
		
		return new ButtonRenderer() {
			@Override
			public void drawButton(Tessellator tessellator, BufferBuilder buffer, MatrixStack matrices) {
				Matrix4f matrix = matrices.peek().getPositionMatrix();
				drawRect.accept(tessellator, buffer, matrix);
				drawBorders.accept(tessellator, buffer, matrix);
			}
			
			@Override
			public void drawText(MatrixStack matrices) {
				fontRenderer.draw(matrices, displayString, textX, textY, prop.buttonTextColor);
			}
		};
	});
	putRF(map, BooleanButton.class, b -> {
		int redfill, greenfill, bluefill;
		if(b.getValue() || b.isHovered()) {
			redfill = prop.brighter(prop.redfill);
			greenfill = prop.brighter(prop.greenfill);
			bluefill = prop.brighter(prop.bluefill);
		} else {
			redfill = prop.redfill;
			greenfill = prop.greenfill;
			bluefill = prop.bluefill;
		}
		float bWidth = prop.buttonBorderWidth;
		int bWidth1 = (int) bWidth, bWidth2 = bWidth1 / 2, x = b.getX() + bWidth2, y = b.getY() + bWidth2,
				width = b.getWidth(), height = b.getHeight(), x2 = x + width - bWidth1, y2 = y + height - bWidth1;
		String displayString = b.getDisplayString();
		float textX = x + (width - fontRenderer.getWidth(displayString)) / 2,
				textY = y + (height - fontRenderer.fontHeight) / 2 + 1;
		TriConsumer<Tessellator, BufferBuilder, Matrix4f> drawRect = drawRect(x, y, x2, y2, zLevel, redfill, greenfill, bluefill, prop.alphafill),
				drawBorders = drawBorders(x, y, x2, y2, bWidth, zLevel, prop.redborder, prop.greenborder, prop.blueborder, prop.alphaborder);
		
		return new ButtonRenderer() {
			@Override
			public void drawButton(Tessellator tessellator, BufferBuilder buffer, MatrixStack matrices) {
				Matrix4f matrix = matrices.peek().getPositionMatrix();
				drawRect.accept(tessellator, buffer, matrix);
				drawBorders.accept(tessellator, buffer, matrix);
			}
			
			@Override
			public void drawText(MatrixStack matrices) {
				fontRenderer.draw(matrices, displayString, textX, textY, prop.buttonTextColor);
			}
		};
	});
	putRF(map, RangeButton.class, b -> {
		float bWidth = prop.buttonBorderWidth;
		int bWidth1 = (int) bWidth, bWidth2 = bWidth1 / 2, x = b.getX() + bWidth2, y = b.getY() + bWidth2,
				width = b.getWidth(), height = b.getHeight(), x3 = x + width - bWidth1, y2 = y + height - bWidth1,
				x2 = x + b.getHighlightWidth() * (x3 - x) / width;
		TriConsumer<Tessellator, BufferBuilder, Matrix4f>
				drawHighlighted = drawRect(x, y, x2, y2, zLevel,
						prop.brighter(prop.redfill), prop.brighter(prop.greenfill), prop.brighter(prop.bluefill), prop.alphafill),
				drawNotHighlighted = drawRect(x2, y, x3, y2, zLevel, prop.redfill, prop.greenfill, prop.bluefill, prop.alphafill),
				drawBorders = drawBorders(x, y, x3, y2, bWidth, zLevel, prop.redborder, prop.greenborder, prop.blueborder, prop.alphaborder);
		String displayString = b.getDisplayString();
		float textX = x + (width - fontRenderer.getWidth(displayString)) / 2,
				textY = y + (height - fontRenderer.fontHeight) / 2 + 1;
		
		return new ButtonRenderer() {
			@Override
			public void drawButton(Tessellator tessellator, BufferBuilder buffer, MatrixStack matrices) {
				Matrix4f matrix = matrices.peek().getPositionMatrix();
				drawHighlighted.accept(tessellator, buffer, matrix);
				drawNotHighlighted.accept(tessellator, buffer, matrix);
				drawBorders.accept(tessellator, buffer, matrix);
			}
			
			@Override
			public void drawText(MatrixStack matrices) {
				fontRenderer.draw(matrices, displayString, textX, textY, prop.buttonTextColor);
			}
		};
	});
	putRF(map, ColorButton.class, b -> {
		int redfill, greenfill, bluefill;
		if(b.isHovered()) {
			redfill = prop.brighter(prop.redfill);
			greenfill = prop.brighter(prop.greenfill);
			bluefill = prop.brighter(prop.bluefill);
		} else {
			redfill = prop.redfill;
			greenfill = prop.greenfill;
			bluefill = prop.bluefill;
		}
		Color value = b.getValue();
		float bWidth = prop.buttonBorderWidth;
		int bWidth1 = (int) bWidth, bWidth2 = bWidth1 / 2, x = b.getX() + bWidth2, y = b.getY() + bWidth2,
				width = b.getWidth(), height = b.getHeight(), x3 = x + width - bWidth1, y2 = y + height - bWidth1,
				renderColorStart = ColorButton.getTextWidth(x3 - x), x2 = x + renderColorStart;
		TriConsumer<Tessellator, BufferBuilder, Matrix4f>
				drawRect = drawRect(x, y, x2, y2, zLevel, redfill, greenfill, bluefill, prop.alphafill),
				drawColor = drawRect(x2, y, x3, y2, zLevel, value.getRed(), value.getGreen(), value.getBlue(), value.getAlpha()),
				drawBorders = drawBorders(x, y, x3, y2, bWidth, zLevel, prop.redborder, prop.greenborder, prop.blueborder, prop.alphaborder);
		String displayString = b.getDisplayString();
		float textX = x + (renderColorStart - fontRenderer.getWidth(displayString)) / 2,
				textY = y + (height - fontRenderer.fontHeight) / 2 + 1;
		
		return new ButtonRenderer() {
			@Override
			public void drawButton(Tessellator tessellator, BufferBuilder buffer, MatrixStack matrices) {
				Matrix4f matrix = matrices.peek().getPositionMatrix();
				drawRect.accept(tessellator, buffer, matrix);
				drawColor.accept(tessellator, buffer, matrix);
				drawBorders.accept(tessellator, buffer, matrix);
			}
			
			@Override
			public void drawText(MatrixStack matrices) {
				fontRenderer.draw(matrices, displayString, textX, textY, prop.buttonTextColor);
			}
		};
	});
	putRF(map, ColorPicker.class, b -> {
		Color value = b.getValue();
		Vec3i saturatedRGB = b.getSaturatedRGB();
		float bWidth = prop.buttonBorderWidth;
		int bWidth1 = (int) bWidth, bWidth2 = bWidth1 / 2, x = b.getX() + bWidth2, y = b.getY() + bWidth2,
				width = b.getWidth(), height = b.getHeight(), x2 = x + width - bWidth1, y2 = y + height - bWidth1,
				triStartHeight = b.getTriStartHeight(), triX = b.getTriX(), triX2 = triX + b.getTriWidth(), triXMid = (triX + triX2) / 2,
				triY = b.getTriY(), triY2 = triY + b.getTriHeight(),
				redS = saturatedRGB.getX(), greenS = saturatedRGB.getY(), blueS = saturatedRGB.getZ();
		TriConsumer<Tessellator, BufferBuilder, Matrix4f>
				drawRect = drawRect(x, y, x2, y2, zLevel, prop.redfill, prop.greenfill, prop.bluefill, prop.alphafill),
				drawColor = drawRect(x, y, x + triStartHeight - bWidth2, y + triStartHeight - bWidth2, zLevel,
						value.getRed(), value.getGreen(), value.getBlue(), value.getAlpha()),
				drawBorders = drawBorders(x, y, x2, y2, bWidth, zLevel, prop.redborder, prop.greenborder, prop.blueborder, prop.alphaborder);
		String displayString = b.getDisplayString();
		float textX = x + (width - fontRenderer.getWidth(displayString)) / 2,
				textY = y + (triStartHeight - fontRenderer.fontHeight) / 2 + 1;
		
		return new ButtonRenderer() {
			@Override
			public void drawButton(Tessellator tessellator, BufferBuilder buffer, MatrixStack matrices) {
				Matrix4f matrix = matrices.peek().getPositionMatrix();
				drawRect.accept(tessellator, buffer, matrix);
				drawColor.accept(tessellator, buffer, matrix);
				buffer.begin(VertexFormat.DrawMode.TRIANGLES, VertexFormats.POSITION_COLOR);
				buffer.vertex(matrix, triXMid, triY, zLevel).color(0x00, 0x00, 0x00, 0xFF).next();
				buffer.vertex(matrix, triX, triY2, zLevel).color(0xFF, 0xFF, 0xFF, 0xFF).next();
				buffer.vertex(matrix, triX2, triY2, zLevel).color(redS, greenS, blueS, 0xFF).next();
				tessellator.draw();
				drawBorders.accept(tessellator, buffer, matrix);
			}
			
			@Override
			public void drawText(MatrixStack matrices) {
				fontRenderer.draw(matrices, displayString, textX, textY, prop.buttonTextColor);
			}
		};
	});
	putRF(map, ColorComponentButton.class, b -> {
		Color minColor = b.getMinColor(), maxColor = b.getMaxColor();
		float bWidth = prop.buttonBorderWidth;
		int redfill = minColor.getRed(), greenfill = minColor.getGreen(), bluefill = minColor.getBlue(),
				redfill1 = maxColor.getRed(), greenfill1 = maxColor.getGreen(), bluefill1 = maxColor.getBlue(),
				alphafill = b.renderAlpha() ? minColor.getAlpha() : ColorComponentButton.MAX_INT,
				alphafill1 = b.renderAlpha() ? maxColor.getAlpha() : ColorComponentButton.MAX_INT,
				bWidth1 = (int) bWidth, bWidth2 = bWidth1 / 2, x = b.getX() + bWidth2, y = b.getY() + bWidth2,
				width = b.getWidth(), height = b.getHeight(), x2 = x + width - bWidth1, y2 = y + height - bWidth1;
		TriConsumer<Tessellator, BufferBuilder, Matrix4f> drawBorders =
				drawBorders(x, y, x2, y2, bWidth, zLevel, prop.redborder, prop.greenborder, prop.blueborder, prop.alphaborder);
		String displayString = b.getDisplayString();
		float textX = x + (width - fontRenderer.getWidth(displayString)) / 2,
				textY = y + (height - fontRenderer.fontHeight) / 2 + 1;
		
		return new ButtonRenderer() {
			@Override
			public void drawButton(Tessellator tessellator, BufferBuilder buffer, MatrixStack matrices) {
				Matrix4f matrix = matrices.peek().getPositionMatrix();
				buffer.begin(VertexFormat.DrawMode.QUADS, VertexFormats.POSITION_COLOR);
				buffer.vertex(matrix, x2, y, zLevel).color(redfill1, greenfill1, bluefill1, alphafill1).next();
				buffer.vertex(matrix, x, y, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
				buffer.vertex(matrix, x, y2, zLevel).color(redfill, greenfill, bluefill, alphafill).next();
				buffer.vertex(matrix, x2, y2, zLevel).color(redfill1, greenfill1, bluefill1, alphafill1).next();
				tessellator.draw();
				drawBorders.accept(tessellator, buffer, matrix);
			}
			
			@Override
			public void drawText(MatrixStack matrices) {
				fontRenderer.draw(matrices, displayString, textX, textY, prop.buttonTextColor);
			}
		};
	});
	putRF(map, ColorComponentButton.HueButton.class, b -> {
		Color minColor = b.getMinColor(), oneThirdColor = b.getOneThirdColor(),
				twoThirdsColor = b.getTwoThirdsColor(), maxColor = b.getMaxColor();
		float bWidth = prop.buttonBorderWidth;
		int redfill = minColor.getRed(), greenfill = minColor.getGreen(), bluefill = minColor.getBlue(),
				redfill1 = oneThirdColor.getRed(), greenfill1 = oneThirdColor.getGreen(), bluefill1 = oneThirdColor.getBlue(),
				redfill2 = twoThirdsColor.getRed(), greenfill2 = twoThirdsColor.getGreen(), bluefill2 = twoThirdsColor.getBlue(),
				redfill3 = maxColor.getRed(), greenfill3 = maxColor.getGreen(), bluefill3 = maxColor.getBlue(),
				bWidth1 = (int) bWidth, bWidth2 = bWidth1 / 2, x = b.getX() + bWidth2, y = b.getY() + bWidth2,
				width = b.getWidth(), height = b.getHeight(), renderWidth = width - bWidth1, x2 = x + renderWidth / 3, y2 = y + height - bWidth1,
				x3 = x + renderWidth * 2 / 3, x4 = x + renderWidth;
		TriConsumer<Tessellator, BufferBuilder, Matrix4f> drawBorders =
				drawBorders(x, y, x4, y2, bWidth, zLevel, prop.redborder, prop.greenborder, prop.blueborder, prop.alphaborder);
		String displayString = b.getDisplayString();
		float textX = x + (width - fontRenderer.getWidth(displayString)) / 2,
				textY = y + (height - fontRenderer.fontHeight) / 2 + 1;
		
		return new ButtonRenderer() {
			@Override
			public void drawButton(Tessellator tessellator, BufferBuilder buffer, MatrixStack matrices) {
				Matrix4f matrix = matrices.peek().getPositionMatrix();
				buffer.begin(VertexFormat.DrawMode.QUADS, VertexFormats.POSITION_COLOR);
				//draw red-green
				buffer.vertex(matrix, x2, y, zLevel).color(redfill1, greenfill1, bluefill1, ColorComponentButton.MAX_INT).next();
				buffer.vertex(matrix, x, y, zLevel).color(redfill, greenfill, bluefill, ColorComponentButton.MAX_INT).next();
				buffer.vertex(matrix, x, y2, zLevel).color(redfill, greenfill, bluefill, ColorComponentButton.MAX_INT).next();
				buffer.vertex(matrix, x2, y2, zLevel).color(redfill1, greenfill1, bluefill1, ColorComponentButton.MAX_INT).next();
				//draw green-blue
				buffer.vertex(matrix, x3, y, zLevel).color(redfill2, greenfill2, bluefill2, ColorComponentButton.MAX_INT).next();
				buffer.vertex(matrix, x2, y, zLevel).color(redfill1, greenfill1, bluefill1, ColorComponentButton.MAX_INT).next();
				buffer.vertex(matrix, x2, y2, zLevel).color(redfill1, greenfill1, bluefill1, ColorComponentButton.MAX_INT).next();
				buffer.vertex(matrix, x3, y2, zLevel).color(redfill2, greenfill2, bluefill2, ColorComponentButton.MAX_INT).next();
				//draw blue-red
				buffer.vertex(matrix, x4, y, zLevel).color(redfill3, greenfill3, bluefill3, ColorComponentButton.MAX_INT).next();
				buffer.vertex(matrix, x3, y, zLevel).color(redfill2, greenfill2, bluefill2, ColorComponentButton.MAX_INT).next();
				buffer.vertex(matrix, x3, y2, zLevel).color(redfill2, greenfill2, bluefill2, ColorComponentButton.MAX_INT).next();
				buffer.vertex(matrix, x4, y2, zLevel).color(redfill3, greenfill3, bluefill3, ColorComponentButton.MAX_INT).next();
				tessellator.draw();
				drawBorders.accept(tessellator, buffer, matrix);
			}
			
			@Override
			public void drawText(MatrixStack matrices) {
				fontRenderer.draw(matrices, displayString, textX, textY, prop.buttonTextColor);
			}
		};
	});
	return map;
}

/**
 * Trims a {@code String} to fit a given width.
 * @param displayString the string to trim
 * @param fontRenderer font renderer
 * @param width maximum width of the trimmed string
 * @param ellipsisWidth width of {@code "..."}
 * @return {@code displayString} or a shortened version of it
 */
public static String trim(String displayString, TextRenderer fontRenderer, int width, int ellipsisWidth) {
	return fontRenderer.getWidth(displayString) > width ?
			fontRenderer.trimToWidth(displayString, width - ellipsisWidth) + "..." :
			displayString;
}

/**
 * Builds a string trimmer for a basic {@link ClickGUIButton}.
 * @param fontRenderer font renderer
 * @param prop properties of buttons
 * @return a {@link Function} that takes in a {@code ClickGUIButton} and {@code String} and produces a {@code String}
 */
public static Function<Bucket<ClickGUIButton, String>, String> simpleStringTrimmer(TextRenderer fontRenderer, int ellipsisWidth) {
	return b -> trim(b.getE2(), fontRenderer, b.getE1().getWidth(), ellipsisWidth);
}

/**
 * Adds all default string trimmers to a map.
 * @param fontRenderer font renderer
 * @param prop properties of buttons
 * @param priorityInheritance map to add prioritized inherited types to
 * @param map map to add string trimmers to
 * @return {@code map}
 */
public static Map<Class<? extends ClickGUIButton>, Function<Bucket<? extends ClickGUIButton, String>, String>> stringTrimmerMap(
		TextRenderer fontRenderer, ButtonProperties prop,
		Map<Class<? extends ClickGUIButton>, Class<? extends ClickGUIButton>> priorityInheritance,
		Map<Class<? extends ClickGUIButton>, Function<Bucket<? extends ClickGUIButton, String>, String>> map) {
	int ellipsisWidth = fontRenderer.getWidth("...");
	putST(map, ClickGUIButton.class, simpleStringTrimmer(fontRenderer, ellipsisWidth));
	putST(map, ColorButton.class, b -> trim(b.getE2(), fontRenderer, ColorButton.getTextWidth(b.getE1().getWidth()), ellipsisWidth));
	return map;
}

/**
 * Contains several property values for rendering buttons.
 * @author Magnileve
 */
public static class ButtonProperties {
	public final int redfill;
	public final int greenfill;
	public final int bluefill;
	public final int alphafill;
	
	public final int redborder;
	public final int greenborder;
	public final int blueborder;
	public final int alphaborder;
	
	public final int buttonTextColor;
	
	public final float buttonBorderWidth;
	
	private final double brightenFactor;
	
	public ButtonProperties(double brightenFactor, int redfill, int greenfill, int bluefill, int alphafill,
			int redborder, int greenborder, int blueborder, int alphaborder, int buttonTextColor, float buttonBorderWidth) {
		this.brightenFactor = brightenFactor;
		this.redfill = redfill;
		this.greenfill = greenfill;
		this.bluefill = bluefill;
		this.alphafill = alphafill;
		this.redborder = redborder;
		this.greenborder = greenborder;
		this.blueborder = blueborder;
		this.alphaborder = alphaborder;
		this.buttonTextColor = buttonTextColor;
		this.buttonBorderWidth = buttonBorderWidth;
	}
	
	/**
	 * Increases the brightness of an RGB byte.
	 * @param brightness a red, blue, or green value from 0-255
	 * @return a brighter red, blue, or green value from 0-255
	 */
	public int brighter(int brightness) {
		int i = (int) (1D / (1D - brightenFactor));
		if(brightness < i) brightness = i;
		return Math.min((int) (brightness / brightenFactor), 255);
	}
}

}