package magnileve.chungamod.gui;

/**
 * A button that can display a message.
 * @author Magnileve
 */
public abstract class PotentialInfoButton extends ClickGUIButtonImpl implements DisplayMessageSender {

/**
 * The {@link UpdatableDisplay} used by this button to display messages.
 */
protected final UpdatableDisplay messageDisplayer;

private boolean isNameTrimmed;
private boolean displayingMessage;

/**
 * Creates a new button.
 * @param id button ID
 * @param x x position
 * @param y y position
 * @param widthIn width
 * @param heightIn height
 * @param name name of button; also the display string unless used differently by a subclass
 * @param rendererFactory factory to build renderer for this button
 * @param messageDisplayer used to display messages
 */
public PotentialInfoButton(int id, int x, int y, int widthIn, int heightIn, String name, ButtonRendererFactory<ClickGUIButton> rendererFactory,
		UpdatableDisplay messageDisplayer) {
	super(id, x, y, widthIn, heightIn, name, rendererFactory);
	this.messageDisplayer = messageDisplayer;
}

@Override
public void displayMessage(String message) {
	messageDisplayer.display(message, this, () -> displayingMessage = false);
	displayingMessage = true;
}

@Override
public void hideDisplayedMessage() {
	if(displayingMessage) messageDisplayer.hide();
}

@Override
protected boolean isNameTrimmed() {
	return isNameTrimmed;
}

/**
 * Called when the mouse moves over or off of this button.
 */
protected void onHover() {
	if(!isHovered()) hideDisplayedMessage();
}

@Override
public boolean updateHovered(double mouseX, double mouseY, boolean alreadyProcessed) {
	boolean oldHovered = hovered, hovered = super.updateHovered(mouseX, mouseY, alreadyProcessed);
	if(oldHovered != hovered) onHover();
	return hovered;
}

@Override
public void setDisplayString(String displayString) {
	super.setDisplayString(displayString);
	setIsNameTrimmed(super.isNameTrimmed());
}

/**
 * Sets the cached value to be returned by {@link #isNameTrimmed()}.
 * @param value whether or not the button name is trimmed
 */
protected void setIsNameTrimmed(boolean value) {
	isNameTrimmed = value;
}

}