package magnileve.chungamod.gui;

import net.minecraft.client.render.BufferBuilder;
import net.minecraft.client.render.Tessellator;
import net.minecraft.client.util.math.MatrixStack;

/**
 * Contains methods to render a button and its text separately.
 * @author Magnileve
 */
public interface ButtonRenderer {

/**
 * Draws this button.
 * @param tessellator instance of tessellator
 * @param buffer rendering buffer
 * @param matrices transforms buttons being rendered
 */
public void drawButton(Tessellator tessellator, BufferBuilder buffer, MatrixStack matrices);

/**
 * Draws text over this button.
 * @param matrices transforms text being rendered
 */
public void drawText(MatrixStack matrices);

/**
 * Renders nothing.
 */
public static final ButtonRenderer BLANK = new ButtonRenderer() {
	@Override public void drawButton(Tessellator tessellator, BufferBuilder buffer, MatrixStack matrices) {}
	@Override public void drawText(MatrixStack matrices) {}
};

}