package magnileve.chungamod.gui;

import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

import org.lwjgl.glfw.GLFW;

import magnileve.chungamod.util.Permit;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.LiteralText;

/**
 * <p>
 * A {@code GuiScreen} composed of instances of core interfaces of the Chungamod GUI API.
 * </p>
 * <p>
 * Most notably, a {@code ClickGUI} contains
 * a {@link ClickGUIButtonBase} list, a {@link MenuChain}, and an {@link UpdatableDisplayButton}.
 * The {@code ClickGUI} manages calls to the buttons for rendering and mouse handling.
 * A {@link Permit} for keyboard and for mouse are also contained and can override default calls on activity.
 * </p>
 * <p>
 * The process for handling mouse activity is as follows:<br>
 * 1. If there is a subGUI as returned by {@link #getSubGUI()}, calls are forwarded to the subGUI.<br>
 * 2. If there is no subGUI, and the mouse permit as returned by {@link #getMousePermit()} is being controlled by an object,
 * calls are forwarded to that object.<br>
 * 3. If there is no subGUI, and the mouse permit is not being controlled by an object,
 * the menu chain as returned by {@link #getSubMenus()} is first notified,
 * and then the buttons in the list returned by {@link #getButtons()} are notified.
 * A copy of the list is made for iteration except for calls to {@link ClickGUIButtonBase#updateHovered(int, int, boolean)}.
 * </p>
 * <p>
 * The process for rendering is the same as above, excluding the mouse permit,
 * except that step 3 is in reverse order, and the display button as returned by {@link #getDisplayer()} is rendered after the rest of step 3.
 * The list of buttons is also iterated directly.
 * </p>
 * @author Magnileve
 * @see ClickGUIButton
 * @see Menu
 */
public class ClickGUI extends Screen {

private final List<ClickGUIButtonBase> buttons;
private final MenuChain subMenus;
private final UpdatableDisplayButton displayer;
private final Permit<MouseHandler> mousePermit;
private final Permit<KeyboardHandler> keyboardPermit;
private final Runnable onEscape;
private final double sizeMultiplier;

private ClickGUI subGUI;

/**
 * Constructs a new {@code ClickGUI}.
 * @param buttons an optionally modifiable list of buttons
 * @param subMenus the start of a menu chain
 * @param displayer an updatable display button
 * @param mousePermit permit to handle mouse activity
 * @param keyboardPermit permit to handle keyboard activity
 * @param closeGUI if not null, runs when the escape key is pressed
 * @param sizeMultiplier constant scale for GUI size
 */
public ClickGUI(List<ClickGUIButtonBase> buttons, MenuChain subMenus, UpdatableDisplayButton displayer,
		Permit<MouseHandler> mousePermit, Permit<KeyboardHandler> keyboardPermit, Runnable closeGUI, double sizeMultiplier) {
	super(new LiteralText("ClickGUI"));
	this.buttons = buttons;
	this.subMenus = subMenus;
	this.displayer = displayer;
	this.mousePermit = mousePermit;
	this.keyboardPermit = keyboardPermit;
	this.onEscape = closeGUI;
	this.sizeMultiplier = sizeMultiplier;
}

/**
 * Renders the components of the GUI.
 * @param matrices transforms objects being rendered
 * @param mouseX x position of mouse
 * @param mouseY y position of mouse
 * @param delta amount of time passed since the last frame
 */
@Override
public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
	if(subGUI != null) {
		subGUI.render(matrices, mouseX, mouseY, delta);
		return;
	}
	float sizeMultiplier = (float) this.sizeMultiplier;
	matrices.push();
	matrices.scale(sizeMultiplier, sizeMultiplier, 1.0F);
	ListIterator<ClickGUIButtonBase> iter = buttons.listIterator(buttons.size());
	while(iter.hasPrevious()) iter.previous().draw(matrices);
	subMenus.draw(matrices);
	displayer.draw(matrices);
	matrices.pop();
}

@Override
public void mouseMoved(double mouseX, double mouseY) {
	if(subGUI == null) {
		mouseX /= sizeMultiplier;
		mouseY /= sizeMultiplier;
		if(mousePermit.isAvailable()) {
			boolean alreadyHovered = subMenus.updateHovered(mouseX, mouseY, false);
			for(ClickGUIButtonBase button:buttons)
				alreadyHovered = button.updateHovered(mouseX, mouseY, alreadyHovered) || alreadyHovered;
			
		} else mousePermit.getController().mouseMoved(mouseX, mouseY);
	} else subGUI.mouseMoved(mouseX, mouseY);
}

@Override
public boolean keyPressed(int keyCode, int scanCode, int modifiers) {
	if(subGUI != null) return subGUI.keyPressed(keyCode, scanCode, modifiers);
	if(keyboardPermit.isAvailable()) return handleKeyPressed(keyCode, scanCode, modifiers);
	return keyboardPermit.getController().keyPressed(keyCode, scanCode, modifiers);
}

/**
 * Handles a captured key down event.
 * This method is called when no sub GUI is present and the keyboard permit is available.
 * @param keyCode the named key code of the event as described in the {@link org.lwjgl.glfw.GLFW GLFW} class
 * @param scanCode the unique/platform-specific scan code of the keyboard input
 * @param modifiers a GLFW bitfield describing the modifier keys that are held down
 * (see <a href="https://www.glfw.org/docs/3.3/group__mods.html">GLFW Modifier key flags</a>)
 * @return {@code true} to indicate that the event handling is successful/valid
 * @see #keyPressed(int, int, int)
 * @see #getSubGUI()
 * @see #getKeyboardPermit()
 */
protected boolean handleKeyPressed(int keyCode, int scanCode, int modifiers) {
	if(keyCode == GLFW.GLFW_KEY_ESCAPE && onEscape != null) {
		onEscape.run();
		return true;
	}
	return super.keyPressed(keyCode, scanCode, modifiers);
}

@Override
public boolean charTyped(char chr, int modifiers) {
	if(subGUI != null) return subGUI.charTyped(chr, modifiers);
	if(keyboardPermit.isAvailable()) return super.charTyped(chr, modifiers);
	keyboardPermit.getController().charTyped(chr, modifiers);
	return true;
}

/**
 * Scales LWJGL mouse position to Mincerfat mouse position on the x axis.
 * @param mouseX a position on the x axis
 * @return the position scaled to Mincerfat measurement
 */
public int scaleMouseX(int mouseX) {
	return (int) (mouseX * width / (double) client.getWindow().getWidth() / sizeMultiplier);
}

/**
 * Scales LWJGL mouse position to Mincerfat mouse position on the y axis.
 * @param mouseY a position on the y axis
 * @return the position scaled to Mincerfat measurement
 */
public int scaleMouseY(int mouseY) {
	return (int) ((height - mouseY * height / (double) client.getWindow().getHeight() - 1) / sizeMultiplier);
}

@Override
public boolean mouseClicked(double mouseX, double mouseY, int mouseButton) {
	if(subGUI != null) return subGUI.mouseClicked(mouseX, mouseY, mouseButton);
	mouseX /= sizeMultiplier;
	mouseY /= sizeMultiplier;
	if(mousePermit.isAvailable()) {
		boolean alreadyProcessed = subMenus.mouseClicked(mouseX, mouseY, mouseButton, false);
		ClickGUIButtonBase[] array = buttons.toArray(new ClickGUIButtonBase[buttons.size()]);
		for(int i = 0; i < array.length; i++)
			alreadyProcessed = array[i].mouseClicked(mouseX, mouseY, mouseButton, alreadyProcessed) || alreadyProcessed;
	} else mousePermit.getController().mouseClicked(mouseX, mouseY, mouseButton);
	return true;
}

@Override
public boolean mouseReleased(double mouseX, double mouseY, int mouseButton) {
	if(subGUI != null) return subGUI.mouseReleased(mouseX, mouseY, mouseButton);
	if(mousePermit.isAvailable()) return super.mouseReleased(mouseX, mouseY, mouseButton);
	mouseX /= sizeMultiplier;
	mouseY /= sizeMultiplier;
	mousePermit.getController().mouseReleased(mouseX, mouseY, mouseButton);
	return true;
}

@Override
public boolean mouseScrolled(double mouseX, double mouseY, double amount) {
	if(subGUI != null) return subGUI.mouseScrolled(mouseX, mouseY, amount);
	mouseX /= sizeMultiplier;
	mouseY /= sizeMultiplier;
	boolean up = amount >= 0;
	if(mousePermit.isAvailable()) {
		boolean alreadyProcessed = subMenus.mouseScrolled(mouseX, mouseY, up, false);
		ClickGUIButtonBase[] array = buttons.toArray(new ClickGUIButtonBase[buttons.size()]);
		for(int i = 0; i < array.length; i++)
			alreadyProcessed = array[i].mouseScrolled(mouseX, mouseY, up, alreadyProcessed) || alreadyProcessed;
	} else mousePermit.getController().mouseScrolled(mouseX, mouseY, up);
	return true;
	
}

/**
 * Gets the list of buttons that make up this {@code ClickGUI}.  This list may or may not be modifiable.
 * @return the list of buttons
 */
public List<ClickGUIButtonBase> getButtons() {
	return buttons;
}

/**
 * Gets the start of this {@code ClickGUI}'s {@link MenuChain}.
 * @return a {@code MenuChain}
 */
public MenuChain getSubMenus() {
	return subMenus;
}

/**
 * Gets this {@code ClickGUI}'s {@link UpdatableDisplayButton}.
 * @return an {@code UpdatableDisplayButton}
 */
public UpdatableDisplayButton getDisplayer() {
	return displayer;
}

/**
 * Gets this {@code ClickGUI}'s mouse permit.
 * If this permit is acquired by an object,
 * the object is notified for any mouse activity instead of other components of this GUI.
 * @return a permit for handling mouse activity
 */
public Permit<MouseHandler> getMousePermit() {
	return mousePermit;
}

/**
 * Gets this {@code ClickGUI}'s keyboard permit.
 * If this permit is acquired by an object, the object is notified for any keyboard activity instead of other components of this GUI.
 * @return a permit for handling keyboard activity
 */
public Permit<KeyboardHandler> getKeyboardPermit() {
	return keyboardPermit;
}

/**
 * Gets the size scale of this {@code ClickGUI}.
 * @return the size scale
 */
public double getSizeMultiplier() {
	return sizeMultiplier;
}

@Override
public boolean shouldPause() {
	return false;
}

/**
 * Gets the {@code subGUI} if it exists.  When a {@code subGUI} exists, rendering and user input are forwarded to it.
 * @return the {@code subGUI}, or {@code null} if it does not exist
 */
public ClickGUI getSubGUI() {
	return subGUI;
}

/**
 * Sets the {@code subGUI}.  When a {@code subGUI} exists, rendering and user input are forwarded to it.
 * @param subGUI the {@code subGUI}, or {@code null} for no {@code subGUI}
 */
public void setSubGUI(ClickGUI subGUI) {
	if(!Objects.equals(this.subGUI, subGUI)) subMenus.closeNext();
	this.subGUI = subGUI;
}

/**
 * Removes the {@code subGUI}.
 * This method is equivalent to the following:
 * <blockquote>
 * {@code setSubGUI(null)}
 * </blockquote>
 * @see #setSubGUI(ClickGUI)
 */
public final void closeSubGUI() {
	setSubGUI(null);
}

/**
 * Gets the contained instance of {@link MinecraftClient}.
 * This is null when the {@code ClickGUI} is constructed but given a value when
 * {@link #setWorldAndResolution(MinecraftClient, int, int)} is called.
 * @return the contained instance of {@link MinecraftClient}
 */
public MinecraftClient getMinecraft() {
	return client;
}

/**
 * Gets the width of the GUI.
 * @return the width of the GUI
 */
public int getWidth() {
	return width;
}

/**
 * Gets the height of the GUI.
 * @return the height of the GUI
 */
public int getHeight() {
	return height;
}

}