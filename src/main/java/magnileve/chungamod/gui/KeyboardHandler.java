package magnileve.chungamod.gui;

/**
 * A {@code KeyboardHandler} handles callbacks related to keyboard actions.
 * @author Magnileve
 */
public interface KeyboardHandler {

/**
 * Callback for when a key down event has been captured.
 * 
 * The key code is identified by the constants in
 * {@link org.lwjgl.glfw.GLFW GLFW} class.
 * 
 * @param keyCode the named key code of the event as described in the {@link org.lwjgl.glfw.GLFW GLFW} class
 * @param scanCode the unique/platform-specific scan code of the keyboard input
 * @param modifiers a GLFW bitfield describing the modifier keys that are held down
 * (see <a href="https://www.glfw.org/docs/3.3/group__mods.html">GLFW Modifier key flags</a>)
 * @return {@code true} to indicate that the event handling is successful/valid
 */
public boolean keyPressed(int keyCode, int scanCode, int modifiers);

/**
 * Callback for when a character input has been captured.
 * 
 * @param chr the captured character
 * @param modifiers a GLFW bitfield describing the modifier keys that are held down
 * (see <a href="https://www.glfw.org/docs/3.3/group__mods.html">GLFW Modifier key flags</a>)
 */
public void charTyped(char chr, int modifiers);

}