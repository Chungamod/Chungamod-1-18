package magnileve.chungamod.gui;

import magnileve.chungamod.util.function.BiDoubleConsumer;

public class InputHandlers {

public static MouseHandler dragAndRelease(BiDoubleConsumer onDrag, MouseButtonProcessor onRelease) {
	return new MouseHandler() {
		@Override
		public void mouseMoved(double mouseX, double mouseY) {
			onDrag.accept(mouseX, mouseY);
		}
		
		@Override
		public void mouseClicked(double mouseX, double mouseY, int mouseButton) {}
		
		@Override
		public void mouseReleased(double mouseX, double mouseY, int mouseButton) {
			onRelease.buttonActivated(mouseX, mouseY, mouseButton);
		}
		
		@Override
		public void mouseScrolled(double mouseX, double mouseY, boolean up) {}
	};
}

@FunctionalInterface
public static interface MouseButtonProcessor {
	public void buttonActivated(double mouseX, double mouseY, int mouseButton);
}

}